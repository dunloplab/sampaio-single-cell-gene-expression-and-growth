clear all, close all

basepath_snaps = '../snapshots/data/';

folders_snaps = dir(basepath_snaps); % Find subfolders that contain data
folders_snaps = folders_snaps(~startsWith({folders_snaps.name}, '.')); % Remove hidden files
folders_snaps = folders_snaps([folders_snaps.isdir]); % Only include directories

basepath_mm = '../mother_machine/data/';
folders_mm = dir(basepath_mm); % Find subfolders that contain data

gene_list = {folders_snaps.name}; % Capture gene names; Use this code to run all data sets
% gene_list{1} = 'gadX';
% gene_list{1} = 'recA';
% gene_list{1} = 'purA';
% gene_list{3} = 'sulA';


% Compare the snapshot and mother machine distributions
swindow = 5;
binedges = 0:0.1:3;
bin_width = 0.11;

plot_i = 1;
for gi = 1:numel(gene_list) % Cycle through all genes
    
    if isequal(gene_list{gi}, 'promoterless')
        disp('Skipping promotorless since data only exists for snaps')
        continue
    end

    % -- Read in all data from snapshots
    files = dir([basepath_snaps, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    disp([gene_list{gi}, ': Loading snap data'])
    for fi = 1:length(files)
        d = load([basepath_snaps gene_list{gi}, '/', files(fi).name]);   
        gfp{fi} = d.data3D(:,6);
    end

    % All data from all replicates for this reporter
    allgfp = vertcat(gfp{:});

    figure(4)
    subplot(4,5,plot_i)
    h = histogram(allgfp/mean(allgfp), binedges, 'FaceAlpha', 0.5, 'Normalization', 'probability'); hold on
    h.BinWidth = bin_width;
    
    clear allgfp gfp
    
    
    % -- Read in all data from movies

    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    fluo_mother_all = [];
    fluo_at_oneFrame = [];
    
    files_mm = dir([basepath_mm, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder  
    
    for fi = 1:numel(files_mm) % Cycle through files
        load([basepath_mm, gene_list{gi}, '/', files_mm(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files_mm(fi).name])
        
        for chamber = 1:numel(res)
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
                
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames:
         
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                fluo_mother = fluo_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
            
            if numel(frames_mother) < Nframes % Only work with mother cells that are in every frame
                continue
            end
            
            %Smooth fluorescence and growth rate
            %Also crop out the first value of each because the first growth
            %rate value is NaN
            fluo_mother = fluo_mother(2:end);
%             fluo_mother = smooth(fluo_mother, swindow);
            
            fluo_at_oneFrame = [fluo_at_oneFrame fluo_mother(1)]; % Get the fluorescense at one frame
        end
        
        if size(fluo_mother,2) > size(fluo_mother,1)
            fluo_mother = fluo_mother';
        end
        fluo_mother_all = [fluo_mother_all; fluo_mother];
        
    end
    
    moma_mean(gi) = mean(fluo_mother_all);
    disp(['Mean of ', gene_list{gi}, ' = ', num2str(moma_mean(gi))])
    
%     histogram(fluo_at_oneFrame/mean(fluo_at_oneFrame), binedges, 'FaceAlpha', 0.5, 'Normalization', 'probability');
    h = histogram(fluo_mother_all/mean(fluo_mother_all), binedges, 'FaceAlpha', 0.5, 'Normalization', 'probability');
    h.BinWidth = bin_width;
    xlim([0 3])
    ylim([0 0.6])
    title(gene_list{gi})
    set(gca, 'YTick', 0:0.25:0.5)
    
    plot_i = plot_i + 1;
    
    clear frame_range
    
end

save moma_mean_vals gene_list moma_mean

% legend('snap', 'frame', 'all')
% legend('snap', 'frame')
legend('snap', 'all')