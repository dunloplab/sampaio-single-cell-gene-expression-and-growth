clear all, close all

basepath_snaps = '../snapshots/data/';

folders_snaps = dir(basepath_snaps); % Find subfolders that contain data
folders_snaps = folders_snaps(~startsWith({folders_snaps.name}, '.')); % Remove hidden files
folders_snaps = folders_snaps([folders_snaps.isdir]); % Only include directories

basepath_mm = '../mother_machine/data/';
folders_mm = dir(basepath_mm); % Find subfolders that contain data

gene_list = {folders_snaps.name}; % Capture gene names; Use this code to run all data sets
% gene_list{1} = 'gadX';
% gene_list{2} = 'araC';
% gene_list{3} = 'recA';

exp_time(1) = 100;
exp_time(2) = 200;
exp_time(3) = 300;
exp_time(4) = 10;
exp_time(5) = 20;
exp_time(6) = 10;
exp_time(7) = 50;
exp_time(8) = 100;
exp_time(9) = 100;
exp_time(10) = 100;
exp_time(11) = 50;
exp_time(12) = 100;
exp_time(13) = 30;
exp_time(14) = 30;
exp_time(15) = 300;
exp_time(16) = 800;

for gi = 1:numel(gene_list)
        
    files = dir([basepath_snaps, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
            
    % Read in all data
    for fi = 1:length(files)
        d = load([basepath_snaps, gene_list{gi}, '/', files(fi).name]);   
        gfp{fi} = d.data3D(:,6);
    end

    % All data from all replicates for this reporter
    allgfp = vertcat(gfp{:});
    
    mean_vals(gi) = mean(allgfp);
    std_vals(gi) = std(allgfp);
            
    clear allgfp gfp
end


CV_vals = std_vals./mean_vals;
% [CV_vals_sort, idx_sort] = sort(CV_vals, 'descend');
CV_vals_sort = CV_vals; % don't actually sort
idx_sort = length(CV_vals):-1:1;

for gi = 1:numel(gene_list)
    figure(1)
    plot(CV_vals_sort(gi), gi, 's'); hold on

    gene_list_sort{gi} = gene_list{idx_sort(gi)};
    
    files_snap = dir([basepath_snaps folders_snaps(gi).name, '/*.mat']);
    
    % Read in all data
    for fi = 1:length(files_snap)
        d = load([basepath_snaps folders_snaps(gi).name, '/', files_snap(fi).name]);   
        gfp{fi} = d.data3D(:,6);
    end
    allgfp = vertcat(gfp{:});
    
    %Calculate tail index
    %Adapted from:
    %https://www.mathworks.com/help/stats/examples/modelling-tail-data-with-the-generalized-pareto-distribution.html
    
    %Find the "exceedances"; i.e. "chop off the tail" for analysis
    gfp_normalized = allgfp / mean_vals(gi);
    tail_threshold = 1 + (std_vals(gi))/mean_vals(gi); %All values above this constitute the tail
    %tail_threshold = quantile(gfp_normalized,.95);
    
    tail = gfp_normalized(gfp_normalized > tail_threshold) - tail_threshold; %Tail is taken and shifted left so it intersects with origin
    n = numel(tail);
    
    gp_param = gpfit(tail); %Fits generalized Pareto distribution to the tail
    gp_k{gi}      = gp_param(1);   % Tail index parameter. Higher kHat = longer tail.
    gp_sigma{gi}  = gp_param(2);   % Scale parameter.
    skew(gi) = skewness(gfp_normalized);
%     disp([gene_list{gi}, ' skewness = ', num2str(skew(gi))])
    
    figure(4) 
    idx = find(idx_sort == gi);
    subplot(4, 5, numel(gene_list)-idx+1)
    bins = 0:.15:3;
    h = bar(bins,histc(tail,bins)/(length(tail)*.15),'histc');
    h.FaceColor = [.9 .9 .9];
    ygrid = linspace(0,1.1*max(tail),100);
    line(ygrid,gppdf(ygrid,gp_k{gi},gp_sigma{gi}));
    xlim([0,3]);
    ylim([0,6]);
    xlabel('Exceedance');
    ylabel('Probability Density');
    title([gene_list{gi}, ', k = ', num2str(round(gp_k{gi},2)),' skew = ', num2str(round(skew(gi),2))])
    
    %End tail index
    
    
    idx = find(idx_sort == gi);
           
    figure(2)
    subplot(numel(gene_list), 1, numel(gene_list)-idx+1)
    h = histogram(allgfp/mean(allgfp));
    % Note: if the next few lines of code cause an error you need to
    % upgrade to Matlab R2020a (or higher)
    h.Normalization = 'probability';
    h.BinWidth = 0.025;
    h.FaceColor = [0.3 0.6 0.7];
    xlim([0 3])
    set(gca, 'XTick', [], 'YTick', [])
    ylabel(gene_list{gi})
    
        figure(2)
    subplot(numel(gene_list), 1, numel(gene_list)-idx+1)
    h = histogram(allgfp/mean(allgfp));
    % Note: if the next few lines of code cause an error you need to
    % upgrade to Matlab R2020a (or higher)
    h.Normalization = 'probability';
    h.BinWidth = 0.025;
    h.FaceColor = [0.3 0.6 0.7];
    xlim([0 3])
    set(gca, 'XTick', [], 'YTick', [])
    ylabel(gene_list{gi})
    
    clear allgfp gfp
end

figure(2)
set(gca, 'XTick', 0:0.5:3)

figure(1)
set(gca, 'YTick', 1:numel(gene_list), 'YTickLabel', gene_list_sort)
xlabel('CV')

figure(5)
subplot(2,2,1); plot(CV_vals, skew, '.')
xlabel('CV'); ylabel('Skewness')

figure(5)
subplot(2, 2, 2); plot(mean_vals, CV_vals.^2, '.')
xlabel('mean'); ylabel('CV^2')

for gi = 1:numel(gene_list)
    disp([gene_list{gi}, ', mean = ' num2str(mean_vals(gi)./exp_time(gi)), ', CV^2 = ', num2str(CV_vals(gi)^2)])
%     disp([gene_list{gi}, ', mean = ' num2str(mean_vals(gi)), ', CV^2 = ', num2str(CV_vals(gi)^2)])
%     disp(gene_list(gi))
end