clear all, close all

names{1} = 'W6'; 
names{2} = 'W6_delta_rpoS';

% names{1} = 'evgA'; 
% names{2} = 'evgA_delta_rpoS';
% names{3} = 'gadW'; 
% names{4} = 'gadW_delta_rpoS';
% names{5} = 'gadX_MG1655';
% names{6} = 'gadX_delta_rpoS';
% names{7} = 'W6'; 
% names{8} = 'W6_delta_rpoS';




um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera

for ni = 1:numel(names)
    load(['pulse_metrics_data_', names{ni}])
    
    figure(1);
    subplot(531); plot(pulse_freq_all_x{1}, pulse_freq_all_y{1}/max(pulse_freq_all_y{1})); hold on; xlim([0 0.4]); ylim([0 1]); title('Freq'); set(gca, 'XTick', 0:0.1:0.4)
    
    fr = numel(find(peaks_per_hour_all{1} ~= 0))/numel(peaks_per_hour_all{1}); % fraction of traces that have at least 2 pulses
    
    subplot(532); plot(pulse_duration_all_x{1}, fr*pulse_duration_all_y{1}); hold on; title('Duration'); ylim([0 0.25]); set(gca, 'XTick', 0:6)
    subplot(533); plot(pulse_amp_all_x{1}, fr*pulse_amp_all_y{1}); hold on; title('Amp'); ylim([0 0.6]); xlim([0 2]); set(gca, 'YTick', 0:0.25:0.5, 'XTick', 0:0.5:2)

%     subplot(532); plot(pulse_duration_all_x{1}, fr*pulse_duration_all_y{1}); hold on; title('Duration'); ylim([0 0.4]); set(gca, 'XTick', 0:6)
%     subplot(533); plot(pulse_amp_all_x{1}, fr*pulse_amp_all_y{1}); hold on; title('Amp'); ylim([0 1]); xlim([0 2]); set(gca, 'XTick', 0:0.5:2)



%     % also load and plot xcorrs here (and autocorrs)
%     load(['xcorr_data_', names{ni}])
%     figure(2)
%     subplot(221)
%     shadedErrorBar(lags_window_right, autox_all, {@mean,@std}, 'lineProps', clist(ni)); hold on
%     xlim([0 lags_window_right(end)]); ylim([-0.5 1]);
%     set(gca, 'XTick', 0:1:4)
%     
%     subplot(222)
%     shadedErrorBar(lags_window, crossxy_all, {@mean,@std}, 'lineProps', clist(ni)); hold on
%     xlim([lags_window(1) lags_window(end)]); ylim([-1 1])
%     grid on
%     set(gca, 'YTick', -1:0.5:1)
    
    figure(3)
    % correct for exposure times
    clear exptime
    switch names{ni}
        case 'gadX'
            exptime = 30;
        case 'gadX_MG1655'
            exptime = 50;
        case 'gadX_delta_rpoS'
            exptime = 50;
        case 'gadX_CM07'
            exptime = 20;
        case 'gadX_integrated'
            exptime = 200;
        case 'evgA'
            exptime = 30;
        case 'evgA_delta_rpoS'
            exptime = 100;
        case 'W6'
            exptime = 325; % note, this is actually 350ms for some and 300ms for others
        case 'W6_delta_rpoS'
            exptime = 100;
        case 'gadW'
            exptime = 10;
        case 'gadW_delta_rpoS'
            exptime = 100;
        case 'recA'
            exptime = 300;
        case 'recA_delta_rpoS'
            exptime = 400;
    end
    subplot(221); plot(fluo_all_x{1}, fluo_all_y{1}); xlim([0 2500]); hold on; title('Fluo')
    subplot(222); plot(um_per_pixel*length_all_x{1}, length_all_y{1}); xlim([0 15]); hold on; title('Length')
    subplot(223); plot(growthrate_all_x{1}, growthrate_all_y{1}); xlim([-0.05 2.5]); hold on; title('Growth Rate')
   

    figure(4)
    subplot(221); errorbar(ni, ksdensitymean(pulse_freq_all_x{1},pulse_freq_all_y{1})); hold on; title('Freq'); ylim([0 0.07]);
    subplot(222); bar(ni, ksdensitymean(pulse_duration_all_x{1}, pulse_duration_all_y{1})); hold on; title('Duration'); ylim([0 2.5])
    subplot(223); bar(ni, ksdensitymean(pulse_amp_all_x{1}, pulse_amp_all_y{1})); hold on; title('Amp')
    
    figure(5)
    subplot(221); bar(ni, ksdensitymean(fluo_all_x{1}, fluo_all_y{1})); hold on; title('Fluo'); ylim([0 2500])
    subplot(222); bar(ni, ksdensitymean(um_per_pixel*length_all_x{1}, length_all_y{1})); hold on; title('Length'); 
    subplot(223); bar(ni, ksdensitymean(growthrate_all_x{1}, growthrate_all_y{1})); hold on; title('Growth Rate'); ylim([0 1.25])
    
    figure(6)
    subplot(221)
    m = ksdensitymean(growthrate_all_x{1},growthrate_all_y{1});
    [clow, chigh] = ksdensity_confidence(growthrate_all_x{1},growthrate_all_y{1});
    errorbar(ni, m, clow-m, chigh-m); hold on
    xlim([0 numel(names)+1])
    ylim([0 1.5])
    
    figure(7)
    subplot(5,2,ni)
    plot(growthrate_all_x{1},growthrate_all_y{1}); hold on
    xlim([0 2.5])
    title(names{ni})
end

figure(6)
set(gca, 'XTick', 1:numel(names), 'XTickLabel', names)

function mx = ksdensitymean(x,y)
v = cumsum(y)/sum(y);
idx = find(v >= 0.5);
mx = x(idx(1));
end

function [clow, chigh] = ksdensity_confidence(x,y)
v = cumsum(y)/sum(y);
idx = find(v >= 0.25);
clow = x(idx(1));
idx = find(v >= 0.75);
chigh = x(idx(1));
end