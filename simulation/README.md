# Stochastic simulation

To run this simulation, you need to be able to compile and run Rust programs.
If needed, visit https://www.rust-lang.org/ for this purpose.

To run the simulation, select the cell type at the beginning of the `main`
function, and run for example the following:

    cargo run --release >output_independent.csv

You can generate like this the four files needed for the analysis:
`output_independent.csv`, `output_growth.csv`, `output_fluo.csv` and
`output_both.csv`.

Once these four files have been created, just run

    python3 plot.py

to produce the plots.
