import pandas as pd
import sys
sys.path.append('../..')
import analysis

def load_data(path):
    df = pd.read_csv(path)
    df_end = df[df.frame == max(df.frame)]
    # shift the time so that frame == 0 is the last frame without antibiotics
    df.frame -= 179
    return df[df.frame == 0]

for suffix in ['independent', 'growth', 'fluorescence', 'both']:
    print(suffix)
    df = load_data(f'output_{suffix}.csv')
    # replace 0 fluorescence with a low value (because we then take the log)
    df.loc[df.fluorescence == 0, 'fluorescence'] = 0.1

    model, trace = analysis.minimalmodel(df)
    analysis.posterior_plot(trace, suffix=suffix)
    analysis.violin_plot(trace, suffix=suffix)
    analysis.posterior_predictive_plots(df, trace, model, suffix=suffix)

