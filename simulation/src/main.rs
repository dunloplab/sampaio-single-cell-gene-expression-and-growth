#![allow(non_snake_case)]
use rand_distr::{Binomial, Distribution};
use rebop::define_system;

define_system! {
    GR0 lambdaGR kon koff km kp lambdamRNA lambdaGFP deathrate _length;
    Cell { GR, Pon, Poff, mRNA, GFP, alive, dead, A }
    r_GR_increase   :       => GR           @ lambdaGR * GR0
    r_GR_decrease   : GR    =>              @ lambdaGR
    r_promoter_on   : Poff  => Pon          @ kon
    r_promoter_off  : Pon   => Poff         @ koff
    r_transcription : Pon   => Pon + mRNA   @ km
    r_translation   : mRNA  => mRNA + GFP   @ kp
    r_decay_mRNA    : mRNA  =>              @ lambdamRNA
    r_decay_GFP     : GFP   =>              @ lambdaGFP
    r_death         : A + alive => A + dead @ deathrate
}

define_system! {
    GR0 lambdaGR kon koff km kp lambdamRNA lambdaGFP deathrate _length;
    CellGR { GR, Pon, Poff, mRNA, GFP, alive, dead, A }
    r_GR_increase   :       => GR           @ lambdaGR * GR0
    r_GR_decrease   : GR    =>              @ lambdaGR
    r_promoter_on   : Poff  => Pon          @ kon
    r_promoter_off  : Pon   => Poff         @ koff
    r_transcription : Pon   => Pon + mRNA   @ km
    r_translation   : mRNA  => mRNA + GFP   @ kp
    r_decay_mRNA    : mRNA  =>              @ lambdamRNA
    r_decay_GFP     : GFP   =>              @ lambdaGFP
    r_death         : A + GR + alive => A + GR + dead         @ deathrate / GR0
}

define_system! {
    GR0 lambdaGR kon koff km kp lambdamRNA lambdaGFP deathrate deg _length;
    CellFluo { GR, Pon, Poff, mRNA, GFP, alive, dead, A }
    r_GR_increase   :       => GR           @ lambdaGR * GR0
    r_GR_decrease   : GR    =>              @ lambdaGR
    r_promoter_on   : Poff  => Pon          @ kon
    r_promoter_off  : Pon   => Poff         @ koff
    r_transcription : Pon   => Pon + mRNA   @ km
    r_translation   : mRNA  => mRNA + GFP   @ kp
    r_decay_mRNA    : mRNA  =>              @ lambdamRNA
    r_decay_GFP     : GFP   =>              @ lambdaGFP
    r_degradation   : GFP + A => GFP        @ deg
    r_death         : A + alive => A + dead @ deathrate
}

define_system! {
    GR0 lambdaGR kon koff km kp lambdamRNA lambdaGFP deathrate deg _length;
    CellBoth { GR, Pon, Poff, mRNA, GFP, alive, dead, A }
    r_GR_increase   :       => GR           @ lambdaGR * GR0
    r_GR_decrease   : GR    =>              @ lambdaGR
    r_promoter_on   : Poff  => Pon          @ kon
    r_promoter_off  : Pon   => Poff         @ koff
    r_transcription : Pon   => Pon + mRNA   @ km
    r_translation   : mRNA  => mRNA + GFP   @ kp
    r_decay_mRNA    : mRNA  =>              @ lambdamRNA
    r_decay_GFP     : GFP   =>              @ lambdaGFP
    r_degradation   : GFP + A => GFP        @ deg
    r_death         : A + GR + alive => A + GR + dead         @ deathrate / GR0
}

fn cell() -> Cell {
    let mut cell = Cell::new();
    cell.seed(3);
    cell.GR0 = 8.;
    cell.lambdaGR = 0.1;
    cell.kon = 1.0;
    cell.koff = 1.0;
    cell.km = 100.0;
    cell.kp = 500.0;
    cell.lambdamRNA = (2f64).ln() / (4.8 / 60.0);
    cell.lambdaGFP = 0.25;
    cell.deathrate = 0.5;
    cell._length = 1.0;
    cell.Pon = 1;
    cell.alive = 1;
    cell.GR = cell.GR0 as isize;
    cell
}

fn cellGR() -> CellGR {
    let mut cell = CellGR::new();
    cell.seed(3);
    cell.GR0 = 8.;
    cell.lambdaGR = 0.1;
    cell.kon = 1.0;
    cell.koff = 1.0;
    cell.km = 100.0;
    cell.kp = 500.0;
    cell.lambdamRNA = (2f64).ln() / (4.8 / 60.0);
    cell.lambdaGFP = 0.25;
    cell.deathrate = 0.5;
    cell._length = 1.0;
    cell.Pon = 1;
    cell.alive = 1;
    cell.GR = cell.GR0 as isize;
    cell
}

fn cellFluo() -> CellFluo {
    let mut cell = CellFluo::new();
    cell.seed(3);
    cell.GR0 = 8.;
    cell.lambdaGR = 0.1;
    cell.kon = 1.0;
    cell.koff = 1.0;
    cell.km = 100.0;
    cell.kp = 500.0;
    cell.lambdamRNA = (2f64).ln() / (4.8 / 60.0);
    cell.lambdaGFP = 0.25;
    cell.deathrate = 2.0;
    cell.deg = 0.001;
    cell._length = 1.0;
    cell.Pon = 1;
    cell.alive = 1;
    cell.GR = cell.GR0 as isize;
    cell
}

fn cellBoth() -> CellBoth {
    let mut cell = CellBoth::new();
    cell.seed(3);
    cell.GR0 = 8.;
    cell.lambdaGR = 0.1;
    cell.kon = 1.0;
    cell.koff = 1.0;
    cell.km = 100.0;
    cell.kp = 500.0;
    cell.lambdamRNA = (2f64).ln() / (4.8 / 60.0);
    cell.lambdaGFP = 0.25;
    cell.deathrate = 2.0;
    cell.deg = 0.001;
    cell._length = 1.0;
    cell.Pon = 1;
    cell.alive = 1;
    cell.GR = cell.GR0 as isize;
    cell
}

macro_rules! impl_growing {
    ($cell:ty) => {
        impl $cell {
            fn grow_until(self: &mut $cell, t: f64, dt: f64) {
                while self.t < t {
                    let gr = self.GR as f64 / self.GR0;
                    self.advance_until(self.t + dt);
                    self._length *= (dt * gr).exp();
                    if self._length > 2. {
                        self.mRNA = Binomial::new(self.mRNA as u64, 0.5)
                            .unwrap()
                            .sample(&mut self.rng) as isize;
                        self.GFP = Binomial::new(self.GFP as u64, 0.5)
                            .unwrap()
                            .sample(&mut self.rng) as isize;
                        self._length /= 2.;
                    }
                }
            }
        }
    };
}

impl_growing!(Cell);
impl_growing!(CellGR);
impl_growing!(CellFluo);
impl_growing!(CellBoth);

fn main() {
    let mut cell = cell(); // for no influence
    //let mut cell = cellGR(); // for growth influence
    //let mut cell = cellFluo(); // for fluorescence influence
    //let mut cell = cellBoth(); // for both influences
    let dt = 0.001;
    let nb_runs = 10000;
    let mut nb_died = 0;
    let mut nb_survived = 0;
    let time_interval_hour = 5.0 / 60.0;
    println!("cell_id,frame,length,growth_rate,fluorescence,alive,outcome");
    for cell_id in 0..nb_runs {
        // resetting state
        cell.t = -20.0; // first frame is at t = 0, so this provides a burn-in time
        cell.A = 0;
        cell.alive = 1;
        cell.dead = 0;
        // starting the experiment
        let mut lengths = Vec::new();
        let mut growths = Vec::new();
        let mut fluos = Vec::new();
        let mut alives = Vec::new();
        // 180 frames without atb
        for frame in 0..180 {
            cell.grow_until(frame as f64 * time_interval_hour, dt);
            lengths.push(cell._length);
            growths.push(cell.GR as f64 / cell.GR0);
            fluos.push(cell.GFP as f64 / cell._length);
            alives.push(cell.alive);
        }
        assert_eq!(cell.alive, 1);
        cell.A = 1;
        // 20 frames with atb
        for frame in 180..200 {
            cell.grow_until(frame as f64 * time_interval_hour, dt);
            lengths.push(cell._length);
            growths.push(cell.GR as f64 / cell.GR0);
            fluos.push(cell.GFP as f64 / cell._length);
            alives.push(cell.alive);
        }
        let outcome = if cell.alive == 1 { 'a' } else { 'x' };
        for frame in 0..200 {
            println!(
                "{cell_id},{frame},{length},{gr},{fluo},{alive},\"{outcome}\"",
                length = lengths[frame],
                gr = growths[frame],
                fluo = fluos[frame],
                alive = alives[frame]
            );
        }
        nb_survived += cell.alive;
        nb_died += cell.dead;
    }
    eprintln!("dead: {nb_died}, survivors: {nb_survived}");
}
