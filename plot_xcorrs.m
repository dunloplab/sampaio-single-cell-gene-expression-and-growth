close all; clear all

basepath = '../mother_machine/data/'; % Directory where data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

% gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets
gene_list{1} = 'zzz-W6';

min_per_frame = 5;
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera

swindow = 5;

xcwindow = 50; % number of frames to include on each side in the cross correlation

clist = jet(numel(gene_list));

cval = [0/255 225/255 125/255]; % color for GFP plots
% cval = [0/255 150/255 200/255]; % color for promoter activity plots

for gi = 1:numel(gene_list) % Cycle through all genes
    
    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.    
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    autox_all = [];
    autoy_all = [];
    crossxy_all = [];
    half_life{gi} = [];
    
    for fi = 1:numel(files) % Cycle through files
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        for chamber = 1:numel(res)
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
            
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames
            length_mother = um_per_pixel*res(chamber).lineage(1).length; % Length
            growthrate_mother = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,1); % Growth rate
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                fluo_mother = fluo_mother(frames_mother);
                length_mother = length_mother(frames_mother);
                growthrate_mother = growthrate_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
            
            if numel(frames_mother) < Nframes % Only work with mother cells that are in every frame
                continue
            end
            
            %Smooth fluorescence and growth rate
            %Also crop out the first value of each because the first growth
            %rate value is NaN
            fluo_mother = fluo_mother(2:end);
            fluo_mother = smooth(fluo_mother, swindow);
            
            length_mother = length_mother(2:end);
            
            growthrate_mother = growthrate_mother(2:end);
            growthrate_mother = smooth(growthrate_mother, swindow);
            
            if mean(growthrate_mother) < 0.5 % Quality control - don't work with cells that aren't growing
                continue
            end
            
            % Derivative calculations
            M = fluo_mother;
            L = length_mother';
            
            dMdt = (M(3:end) - M(1:end-2))/(2*min_per_frame/60);
            dLdt = (L(3:end) - L(1:end-2))/(2*min_per_frame/60);
            
            dLdt = remove_neg(dLdt);
            
            M = M(2:end-1);
            L = L(2:end-1);
            mu = growthrate_mother(2:end-1);
            
            p = 0.1;
            PA = M.*((1./L).*dLdt + p) + dMdt; % promoter activity
            
            PA = smooth(PA, swindow);
            mu = smooth(mu, swindow);
            
            % Calculate cross/auto correlations
                        
%             % GFP Fluorescence vs. growth rate
            A = fluo_mother;
            B = growthrate_mother;
            
            % Promoter activity vs. growth rate
%             A = PA;
%             B = mu;
            
            autox = xcov(A, 'normalized');
            autoy = xcov(B, 'normalized');
            crossxy = xcov(A, B, 'normalized');
            
            lags = min_per_frame/60*[-(numel(crossxy)-1)/2:(numel(crossxy)-1)/2]; % Calculate time-lag values
            
            window_idx       = ((numel(autox)-1)/2 + 1) + [-xcwindow:xcwindow];  % the window of indexes we actually care about
            window_idx_right = ((numel(autox)-1)/2 + 1) + [0:xcwindow]; % just the positive / right hand side values
            
            lags_window = lags(window_idx);
            lags_window_right = lags(window_idx_right);
            
            autox_all(end+1,:) = autox(window_idx_right);
            autoy_all(end+1,:) = autoy(window_idx_right);
            crossxy_all(end+1,:) = crossxy(window_idx);
            
            % Calculate the half life by finding the lag time that corresponds to the 0.5 crossing
            x = autox(window_idx_right);
            l = lags_window_right(x >= 0.5);
            half_life{gi} = [half_life{gi} max(l)];
        end
        
    end
    
    
    figure(1)
    subplot(4,5,gi)
%         subplot(2,2,1)
%         plot(lags_window_right, autox_all, 'Color', [0.5 0.5 0.5]); hold on
%         plot(lags_window_right, mean(autox_all), 'Color', clist(gi,:), 'LineWidth', 2);
    shadedErrorBar(lags_window_right, autox_all, {@mean,@std}, 'lineprops', {'Color', cval})
    title(gene_list{gi})
    xlim([0 lags_window_right(end)]); ylim([-0.5 1])
    set(gca, 'XTick', 0:1:4)
    
    figure(2)
    subplot(4,5,gi)
%         plot(lags_window, crossxy_all, 'Color', [0.5 0.5 0.5]); hold on
%         plot(lags_window, mean(crossxy_all), 'k', 'LineWidth', 2)
    shadedErrorBar(lags_window, crossxy_all, {@mean,@std}, 'lineprops', {'Color', cval})
    title(gene_list{gi})
    xlim([lags_window(1) lags_window(end)]); ylim([-1 1])
    grid on
    set(gca, 'YTick', -1:0.5:1)
    
    crossxy_allgenes{gi} = crossxy_all;
    m = mean(crossxy_all);
    [~, idx] = max(abs(m));
    crossxy_min_tau(gi, :) = [m(idx) lags_window(idx)];
    
    clear frame_range
    
%     save xcorr_data_W6 lags_window lags_window_right autox_all crossxy_all
end

figure(3)
% [~, sidx] = sort(crossxy_min_tau(:,1), 'descend'); % sort
sidx = numel(gene_list):-1:1; % don't sort

for gi = 1:numel(gene_list)
    subplot(numel(gene_list),1,numel(gene_list) - gi + 1)
    %     plot(lags_window, mean(crossxy_allgenes{sidx(gi)}), 'LineWidth', 2);
    shadedErrorBar(lags_window, crossxy_allgenes{sidx(gi)}, {@mean,@std}, 'lineProps', 'b-')
    xlim(lags_window([1 end]))
    ylim([-1 1])
    set(gca, 'XTickLabel', [], 'YTickLabel', [])
    ylabel(gene_list{sidx(gi)})
    grid on
    gene_list_sort{gi} = gene_list{sidx(gi)}; % list of sorted names
end

figure(6)
subplot(211)
plot(1:numel(gene_list), crossxy_min_tau(:,1), 'o', 'Color', cval)
set(gca, 'XTick', 1:numel(gene_list), 'XTickLabel', gene_list); xlim([0 numel(gene_list)+1]); ylim([-0.75 0.25])
title('R extreme')
set(gca, 'YTick', -1:0.25:0.25)

figure(7)
subplot(122)
plot(crossxy_min_tau(sidx,2), 1:numel(gene_list), 'o', 'Color', cval)
set(gca, 'XTick', -0.75:0.25:0.75, 'YTick', 1:numel(gene_list), 'YTickLabel',gene_list_sort); ylim([0 numel(gene_list)+1]); xlim([-0.75 0.75])
title('tau extreme')

figure(8)
subplot(221)
plot(crossxy_min_tau(:,2), crossxy_min_tau(:,1), 'o', 'Color', cval)
xlabel('tau extreme'); ylabel('R extreme')
axis([-0.75 0.75 -0.75 0.75])
set(gca, 'XTick', -0.75:0.25:0.75, 'YTick', -0.75:0.25:0.75)
axis square

save xcorr_data_summary gene_list half_life crossxy_min_tau

figure(4)
subplot(211)
x = gene_list; % save the original gene_list order before loading division time data
load division_time_data
gene_list_div = gene_list;
gene_list = x;
for gi = 1:numel(half_life)
    if isequal(gene_list, gene_list_div)
        errorbar(gi, mean(half_life{gi}), std(half_life{gi}), 'bo'); hold on
        errorbar(gi, nanmean(all_div_means{gi}/60), nanstd(all_div_means{gi}/60), 'rx')
        %         errorbar(gi, mean(half_life{gi}), std(half_life{gi})/sqrt(sum(~isnan(half_life{gi}))), 'bo'); hold on
        %         errorbar(gi, nanmean(all_div_means{gi}), nanstd(all_div_means{gi})/sqrt(sum(~isnan(all_div_means{gi}))), 'rx')
    else
        %%% NEED to write code here that re-sorts if this isn't true
        disp('gene_list orders are not identical')
    end
end
set(gca, 'XTick', 1:numel(gene_list), 'XTickLabel', gene_list, 'YTick', 0:0.5:3.5); xlim([0 numel(gene_list)+1]); ylim([0 3.5])
