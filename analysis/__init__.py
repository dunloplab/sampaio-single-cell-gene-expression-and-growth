import numpy as np
import pymc3 as pm
import seaborn as sns
import pandas as pd
import arviz as az
import arviz.labels as azl
from scipy.io import loadmat
import matplotlib.pyplot as plt
from scipy.special import expit as invlogit
from pathlib import Path
from scipy.sparse import coo_matrix
from scipy.signal import convolve2d
from tqdm import tqdm

TIME_INTERVAL_MIN = 5
RANDOM_SEED = 0

def load_data(gene, all_times=False):
    print(f'[gadX] loading data for {gene}...')
    timepoints = {
            'gadX1': 50,
            'gadX2': 171,
            'gadX3': 201,
            'gadX4': 199,
            'bolA': 235,
            'recA2': 203,
            'W6_1': 204,
            'W6_2': 207,
            }
    df_lengths = []
    df_growths = []
    df_fluos = []
    df_outcomes = []
    df_cellids = []
    df_frames = []

    cellid = 0
    for path in tqdm(sorted(Path('.').glob(f'{gene}*/*_outcomes.mat'))):
        if str(path.parent) in ['gadX1', 'recA1']:
            continue
        data = loadmat(path)
        res = data['res'][0]
        for pos in res:
            if len(pos) <= 3:
                continue
            if pos[3].shape != (1,):
                continue
            outcome = pos[3][0]
            if outcome == 'z':
                # error with outcome
                continue
            assert outcome in 'afx'
            lineage = pos[0][0]
            framenbs = lineage['framenbs'][0][0]
            lengths = lineage['length'][0][0]
            fluo1 = lineage['fluo1'][0][0]
            daughters = lineage['daughters'][0][0]
            grs = []
            for iframe in range(len(lengths)):
                if iframe == 0:
                    l0 = lengths[iframe]
                else:
                    lm1 = lengths[iframe-1]
                    if daughters[iframe] != 0:
                        sisternb = daughters[iframe] - 1
                        assert lineage['framenbs'][sisternb][0][0] == iframe + 1
                        sisterlength = lineage['length'][sisternb][0][0]
                        lm1 *= lengths[iframe] / (lengths[iframe] + sisterlength)
                if iframe == len(lengths) - 1:
                    l0 = lengths[iframe]
                else:
                    lp1 = lengths[iframe+1]
                    if daughters[iframe+1] != 0:
                        sisternb = daughters[iframe+1] - 1
                        assert lineage['framenbs'][sisternb][0][0] == iframe + 2
                        sisterlength = lineage['length'][sisternb][0][0]
                        lp1 += sisterlength
                if iframe == 0:
                    gr = np.log(lp1 / l0)
                elif iframe == len(lengths) - 1:
                    gr = np.log(l0 / lm1)
                else:
                    gr = np.log(lp1 / lm1) / 2
                grs.append(gr / (TIME_INTERVAL_MIN / 60))

            df_lengths.append(lengths)
            df_fluos.append(fluo1)
            df_growths.append(grs)
            df_outcomes.append([outcome] * len(lengths))
            df_frames.append(np.array(framenbs, dtype=np.int16) - timepoints[str(path.parent)])
            df_cellids.append([cellid] * len(lengths))
            cellid += 1

    df_all_times = pd.DataFrame({
        'length': np.concatenate(df_lengths),
        'growth_rate': np.concatenate(df_growths),
        'fluorescence': np.concatenate(df_fluos),
        'outcome': pd.Series(np.concatenate(df_outcomes), dtype='category'),
        'frame': np.concatenate(df_frames),
        'cell_id': np.concatenate(df_cellids),
        })

    if all_times:
        return df_all_times
    return df_all_times[df_all_times.frame == 0]


def minimalmodel(df, seed=RANDOM_SEED):
    mean_gr = df.growth_rate.mean()
    std_gr = df.growth_rate.std()
    mean_logf = np.log(df.fluorescence).mean()
    df_std = pd.DataFrame({
        'mu': (df[df.outcome != 'f'].growth_rate - mean_gr) / std_gr,
        'F': df[df.outcome != 'f'].fluorescence / np.exp(mean_logf),
        'S': df[df.outcome != 'f'].outcome == 'a',
        })

    with pm.Model() as minimal_model:
        mu = pm.Normal("mu", 0, 1, observed=df_std.mu)
        betaFmu = pm.Normal("betaFmu", 0, 5)
        sigmaF = pm.Exponential("sigmaF", 1)
        F = pm.Lognormal("F", betaFmu*mu, sigmaF, observed=df_std.F)
        alphaS = pm.Normal("alphaS", 0, 5)
        betaSmu = pm.Normal("betaSmu", 0, 5)
        betaSF = pm.Normal("betaSF", 0, 5)
        p = pm.invlogit(alphaS + betaSmu * mu + betaSF * np.log(F))
        S = pm.Bernoulli("S", p, observed=df_std.S)
        trace = pm.sample(tune=1000, draws=1000, cores=4, return_inferencedata=True, random_seed=seed)
        print(az.summary(trace))
        return minimal_model, trace


def posterior_plot(trace, suffix=''):
    print('[gadX] posterior')
    labeller = azl.MapLabeller(var_name_map={
        'alphaS': r'$\alpha$',
        'betaFmu': r'$\gamma$',
        'betaSmu': r'$\beta_\mu$',
        'betaSF': r'$\beta_F$',
        'sigmaF': r'$\sigma$',
        })
    az.plot_pair(trace, marginals=True, kind='kde', figsize=(10, 10),
            marginal_kwargs={'quantiles': [.03, .5, .97]}, labeller=labeller)
    plt.savefig(f'posterior{"_" + suffix if suffix else ""}.pdf', bbox_inches='tight')


def _find_hdi_contours(density, hdi_probs):
    """
    From Arviz.
    Find contours enclosing regions of highest posterior density.
    Parameters
    ----------
    density : array-like
        A 2D KDE on a grid with cells of equal area.
    hdi_probs : array-like
        An array of highest density interval confidence probabilities.
    Returns
    -------
    contour_levels : array
        The contour levels corresponding to the given HDI probabilities.
    """
    # Using the algorithm from corner.py
    sorted_density = np.sort(density, axis=None)[::-1]
    sm = sorted_density.cumsum()
    sm /= sm[-1]

    contours = np.empty_like(hdi_probs)
    for idx, hdi_prob in enumerate(hdi_probs):
        try:
            contours[idx] = sorted_density[sm <= hdi_prob][-1]
        except IndexError:
            contours[idx] = sorted_density[0]

    return contours

def _stack(x, y):
    assert x.shape[1:] == y.shape[1:]
    return np.vstack((x, y))

def _cov_1d(x):
    x = x - x.mean(axis=0)
    ddof = x.shape[0] - 1
    return np.dot(x.T, x.conj()) / ddof


def _cov(data):
    if data.ndim == 1:
        return _cov_1d(data)
    elif data.ndim == 2:
        x = data.astype(float)
        avg, _ = np.average(x, axis=1, weights=None, returned=True)
        ddof = x.shape[1] - 1
        if ddof <= 0:
            warnings.warn("Degrees of freedom <= 0 for slice", RuntimeWarning, stacklevel=2)
            ddof = 0.0
        x -= avg[:, None]
        prod = _dot(x, x.T.conj())
        prod *= np.true_divide(1, ddof)
        prod = prod.squeeze()
        prod += 1e-6 * np.eye(prod.shape[0])
        return prod
    else:
        raise ValueError(f"{data.ndim} dimension arrays are not supported")

def _dot(x, y):
    return np.dot(x, y)

def _fast_kde_2d(x, y, gridsize=(128, 128), circular=False):
    """
    From Arviz.
    2D fft-based Gaussian kernel density estimate (KDE).
    The code was adapted from https://github.com/mfouesneau/faststats
    Parameters
    ----------
    x : Numpy array or list
    y : Numpy array or list
    gridsize : tuple
        Number of points used to discretize data. Use powers of 2 for fft optimization
    circular: bool
        If True use circular boundaries. Defaults to False
    Returns
    -------
    grid: A gridded 2D KDE of the input points (x, y)
    xmin: minimum value of x
    xmax: maximum value of x
    ymin: minimum value of y
    ymax: maximum value of y
    """
    x = np.asarray(x, dtype=float)
    x = x[np.isfinite(x)]
    y = np.asarray(y, dtype=float)
    y = y[np.isfinite(y)]

    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()

    len_x = len(x)
    weights = np.ones(len_x)
    n_x, n_y = gridsize

    d_x = (xmax - xmin) / (n_x - 1)
    d_y = (ymax - ymin) / (n_y - 1)

    xyi = _stack(x, y).T
    xyi -= [xmin, ymin]
    xyi /= [d_x, d_y]
    xyi = np.floor(xyi, xyi).T

    scotts_factor = len_x ** (-1 / 6)
    cov = _cov(xyi)
    std_devs = np.diag(cov) ** 0.5
    kern_nx, kern_ny = np.round(scotts_factor * 2 * np.pi * std_devs)

    inv_cov = np.linalg.inv(cov * scotts_factor ** 2)

    x_x = np.arange(kern_nx) - kern_nx / 2
    y_y = np.arange(kern_ny) - kern_ny / 2
    x_x, y_y = np.meshgrid(x_x, y_y)

    kernel = _stack(x_x.flatten(), y_y.flatten())
    kernel = _dot(inv_cov, kernel) * kernel
    kernel = np.exp(-kernel.sum(axis=0) / 2)
    kernel = kernel.reshape((int(kern_ny), int(kern_nx)))

    boundary = "wrap" if circular else "symm"

    grid = coo_matrix((weights, xyi), shape=(n_x, n_y)).toarray()
    grid = convolve2d(grid, kernel, mode="same", boundary=boundary)

    norm_factor = np.linalg.det(2 * np.pi * cov * scotts_factor ** 2)
    norm_factor = len_x * d_x * d_y * norm_factor ** 0.5

    grid /= norm_factor

    return grid, xmin, xmax, ymin, ymax


def violin_plot(trace, suffix=''):
    fig, axs = plt.subplots(1, 3, figsize=(4, 4))
    labeller = azl.MapLabeller(var_name_map={
        'alphaS': r'$\alpha$',
        'betaFmu': r'$\gamma$',
        'betaSmu': r'$\beta_\mu$',
        'betaSF': r'$\beta_F$',
        'sigmaF': r'$\sigma$',
        })
    if suffix in ['independent', 'growth', 'fluorescence', 'both']:
        axs[0].set_ylim(-0.5, 0.5)
        axs[1].set_ylim(-2, 2)
        axs[2].set_ylim(-2, 2)
    else:
        axs[0].set_ylim(-1, 1)
        axs[1].set_ylim(-5, 10)
        axs[2].set_ylim(-5, 10)
    for ax in axs:
        ax.axhline(0, color='gray')
    fig.tight_layout(pad=1)
    az.plot_violin(trace, var_names=['betaFmu'], quartiles=False, ax=axs[0], labeller=labeller)
    az.plot_violin(trace, var_names=['betaSmu'], quartiles=False, ax=axs[1], labeller=labeller)
    az.plot_violin(trace, var_names=['betaSF'], quartiles=False, ax=axs[2], labeller=labeller)
    plt.savefig(f'violin_plot{"_" + suffix if suffix else ""}.pdf', bbox_inches='tight')



def posterior_predictive_plots(df, trace, model, suffix='', seed=RANDOM_SEED):
    post_alphaS = np.array(trace.posterior.alphaS).ravel()
    post_betaFmu = np.array(trace.posterior.betaFmu).ravel()
    post_sigmaF = np.array(trace.posterior.sigmaF).ravel()
    post_betaSmu = np.array(trace.posterior.betaSmu).ravel()
    post_betaSF = np.array(trace.posterior.betaSF).ravel()

    with model:
        post_pred = pm.sample_posterior_predictive(trace, random_seed=seed)

    df_pp = pd.DataFrame({name: value.ravel() for name, value in post_pred.items()})

    mean_gr = df.growth_rate.mean()
    std_gr = df.growth_rate.std()
    mean_logf = np.log(df.fluorescence).mean()

    df_alive = df[df.outcome == 'a']
    df_alive200 = df_alive.sample(min(200, len(df_alive)), replace=False, random_state=seed)
    df_alive100 = df_alive.sample(min(100, len(df_alive)), replace=False, random_state=seed)
    df_dead = df[df.outcome == 'x']
    df_dead200 = df_dead.sample(min(200, len(df_dead)), replace=False, random_state=seed)
    df_dead100 = df_dead.sample(min(100, len(df_dead)), replace=False, random_state=seed)

    post_pred_alive = df_pp[df_pp.S == 1]
    post_pred_alive200 = post_pred_alive.sample(
            min(200, len(post_pred_alive)), replace=False, random_state=seed)
    post_pred_dead = df_pp[df_pp.S == 0]
    post_pred_dead200 = post_pred_dead.sample(
            min(200, len(post_pred_dead)), replace=False, random_state=seed)

    print('[gadX] posterior predictive')
    fig, ax = plt.subplots()

    fmin = 5e2
    fmax = 2e4
    if suffix == 'W6':
        fmin = 1e3
    elif suffix in ['independent', 'growth', 'fluorescence', 'both']:
        fmin = 3e1
        fmax = 9e3
    fs = np.logspace(np.log10(fmin), np.log10(fmax), 101)
    mumin = -0.5
    mumax = 2.4
    if suffix == 'bolA' or suffix == 'recA':
        mumax = 1.8
    elif suffix == 'W6':
        mumax = 1.2
    elif suffix in ['independent', 'growth', 'fluorescence', 'both']:
        mumin = 0
    mus = np.linspace(mumin, mumax)

    grid = post_alphaS[np.newaxis, np.newaxis, :] + post_betaSmu[np.newaxis, np.newaxis, :] * (mus[:, np.newaxis, np.newaxis]-mean_gr)/std_gr + post_betaSF[np.newaxis, np.newaxis, :] * np.log(fs[np.newaxis, :, np.newaxis] / np.exp(mean_logf))
    ax.contourf(mus, fs, (grid > 0).mean(axis=2).T, levels=[0.1, 0.9], colors='gray', alpha=0.3)
    ax.contour(mus, fs, (grid > 0).mean(axis=2).T, levels=[0.5], colors='black')

    ax.plot([], label='Survived (post. pred.)', color='C2')
    ax.plot([], label='Died (post. pred.)', color='gray')
    hdi_contours = [.2, .4, .6, .8, 1]
    sns.kdeplot(
            post_pred_alive200.mu * std_gr + mean_gr,
            y=post_pred_alive200.F * np.exp(mean_logf),
            log_scale=(False, True), levels=hdi_contours, color='C2', ax=ax)
    sns.kdeplot(
            post_pred_dead200.mu * std_gr + mean_gr,
            y=post_pred_dead200.F * np.exp(mean_logf),
            log_scale=(False, True), levels=hdi_contours, color='gray', ax=ax)
    ax.scatter(
            df_alive100.growth_rate,
            df_alive100.fluorescence,
            label='Survived (exp.)', color='C2')
    ax.scatter(
            df_dead100.growth_rate,
            df_dead100.fluorescence,
            label='Died (exp.)', color='gray', marker='x')
    ax.plot([], color='black', label='Survival probability')

    ax.set_ylabel('log')
    ax.set_xlim(mumin, mumax)
    ax.set_ylim(fmin, fmax)
    #ax.legend(loc='upper right')
    ax.grid()
    ax.set_xlabel('Growth rate at t = 0 (1/h)')
    ax.set_ylabel('GFP at t = 0')
    plt.savefig(f'posterior_predictive{"_" + suffix if suffix else ""}.pdf', bbox_inches='tight')

    print('[gadX] posterior prediction')
    plt.figure()

    df120 = df[df.outcome != 'f'].sample(min(120, len(df[df.outcome != 'f'])), random_state=seed)
    ps = invlogit(
            np.array(trace.posterior.alphaS)[:, :, np.newaxis]
            + np.array(trace.posterior.betaSmu)[:, :, np.newaxis] * (np.array(df120.growth_rate)[np.newaxis, np.newaxis, :] - mean_gr)/std_gr
            + np.array(trace.posterior.betaSF)[:, :, np.newaxis] * np.log(np.array(df120.fluorescence)[np.newaxis, np.newaxis, :] / np.exp(mean_logf)))
    df120['mean_p'] = ps.mean(axis=0).mean(axis=0)
    df120['lb_p'], df120['ub_p'] = az.hdi(ps).T
    df120.sort_values('mean_p', inplace=True, ignore_index=True)
    df120_alive = df120[df120.outcome == 'a']
    df120_dead = df120[df120.outcome == 'x']
    plt.errorbar(
            df120_alive.index,
            df120_alive.mean_p,
            yerr=(df120_alive.mean_p - df120_alive.lb_p, df120_alive.ub_p - df120_alive.mean_p),
            fmt='o', color='C2', label='Survived')
    plt.errorbar(
            df120_dead.index,
            df120_dead.mean_p,
            yerr=(df120_dead.mean_p - df120_dead.lb_p, df120_dead.ub_p - df120_dead.mean_p),
            fmt='x', color='gray', label='Died')
    plt.legend(loc='upper left')
    plt.ylabel('predicted survival probability and 94% HDI')
    plt.xlabel('cells')
    plt.ylim(0, 1)
    plt.savefig(f'posterior_prediction{"_" + suffix if suffix else ""}.pdf', bbox_inches='tight')

    print('[gadX] counterfactual')
    plt.figure()
    if suffix in ['independent', 'growth', 'fluorescence', 'both']:
        counterfactual_fs = np.logspace(np.log10(3e1), np.log10(9e3)) / np.exp(mean_logf)
    else:
        counterfactual_fs = np.logspace(
                np.log10(df.fluorescence.min()),
                np.log10(df.fluorescence.max())) / np.exp(mean_logf)
    counterfactual_logitps = np.array(trace.posterior.alphaS)[:, :, np.newaxis] + np.array(trace.posterior.betaSF)[:, :, np.newaxis] * np.log(counterfactual_fs[np.newaxis, np.newaxis, :])
    mean_counterfactual_logitps = counterfactual_logitps.mean(axis=0).mean(axis=0)
    hdi_data = az.hdi(counterfactual_logitps)
    plt.plot(counterfactual_fs * np.exp(mean_logf), mean_counterfactual_logitps)
    plt.fill_between(counterfactual_fs * np.exp(mean_logf), hdi_data[:, 0], hdi_data[:, 1], color='C1', alpha=0.5, label='94% HDI')
    plt.xscale('log')
    plt.xlabel('manipulated fluorescence [au], growth rate held at mean')
    plt.ylabel('logit(survival probability)')
    plt.title('Counterfactual effect of the fluorescence on the survival probability')
    plt.legend()
    plt.savefig(f'counterfactual_fluorescence{"_" + suffix if suffix else ""}.pdf', bbox_inches='tight')


def history_plot(gene):
    df = analysis.load_data(gene, all_times=True)
    traces = {}
    for i in range(0, 121, 3):
        print(f'{i} frames into the past')
        dft = df[df.frame == -i]
        model, trace = analysis.minimalmodel(dft)
        traces[i] = trace

    betaSmu_means = [trace.posterior.betaSmu.mean() for trace in traces.values()]
    betaSmu_lb_ub = [trace.posterior.betaSmu.quantile([0.03, 0.97]) for trace in traces.values()]
    betaSF_means = [trace.posterior.betaSF.mean() for trace in traces.values()]
    betaSF_lb_ub = [trace.posterior.betaSF.quantile([0.03, 0.97]) for trace in traces.values()]
    betaFmu_means = [trace.posterior.betaFmu.mean() for trace in traces.values()]
    betaFmu_lb_ub = [trace.posterior.betaFmu.quantile([0.03, 0.97]) for trace in traces.values()]

    fig, axs = plt.subplots(3, 1, sharex=True)
    times = [-t * (5 / 60) for t in traces.keys()]
    axs[0].fill_between(times,
            [lb for lb, _ in betaSmu_lb_ub], [ub for _, ub in betaSmu_lb_ub], alpha=0.3)
    axs[0].plot(times, betaSmu_means, label='betaSmu')
    axs[1].fill_between(times,
            [lb for lb, _ in betaSF_lb_ub], [ub for _, ub in betaSF_lb_ub], alpha=0.3)
    axs[1].plot(times, betaSF_means, label='betaSF')
    axs[2].fill_between(times,
            [lb for lb, _ in betaFmu_lb_ub], [ub for _, ub in betaFmu_lb_ub], alpha=0.3)
    axs[2].plot(times, betaFmu_means, label='betaFmu')
    for ax in axs:
        ax.legend()
        ax.grid()
    fig.savefig(f'history_{gene}.pdf', bbox_inches='tight')

