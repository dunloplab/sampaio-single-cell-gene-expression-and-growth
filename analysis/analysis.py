import matplotlib.pyplot as plt
import analysis

for gene in ['gadX', 'recA', 'bolA', 'W6']:
    df = analysis.load_data(gene)
    print(len(df[df.outcome == 'a']), 'survived')
    print(len(df[df.outcome == 'f']), 'filamented')
    print(len(df[df.outcome == 'x']), 'died')
    model, trace = analysis.minimalmodel(df)
    analysis.violin_plot(trace, suffix=gene)
    analysis.posterior_plot(trace, suffix=gene)
    analysis.posterior_predictive_plots(df, trace, model, suffix=gene)

# # Uncomment this to plot Fig. S17
# analysis.history_plot('gadX')
