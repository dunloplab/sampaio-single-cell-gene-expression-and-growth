# Statistical analysis

To reproduce the plots of Fig. 4, place this file at the same level as the
experiment directories (`gadX2`, `gadX3`, `gadX4`, `recA2`, etc.), and
arrange the python path for it to be able to import the module `analysis`.
