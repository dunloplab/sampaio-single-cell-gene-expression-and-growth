close all; clear all

basepath = '../mother_machine/data/'; % Directory where data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

% gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets
gene_list{1} = 'gadX';
% gene_list{1} = 'recA';
% gene_list{1} = 'gadX_CM07';
% gene_list{1} = 'gadX_delta_rpoS';
% gene_list{1} = 'gadX_MG1655';
% gene_list{1} = 'gadX_integrated';
% gene_list{2} = 'rpoH'; % Use this code instead if you want to run just one (or a few) data sets -- comment out gene_list code above
% gene_list{2} = 'araC';
% gene_list{1} = 'recA';
% gene_list{2} = 'metJ';



offset = 1; % set to 0 to have plots of top of each other, 1 for spaced out plots

min_per_frame = 5;
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera

swindow = 5; % smoothing window

clist = jet(numel(gene_list)); % generate color list

for gi = 1:numel(gene_list)

    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.    
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    peaks_per_hour = [];
    peak_duration = [];
    peak_amplitude = [];
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
%     
%     autox_all = [];
%     autoy_all = [];
%     crossxy_all = [];
    
    fluo_mother_all = [];
    length_mother_all = [];
    growthrate_mother_all = [];
    fluo_mother_all_matrix = [];
    
    lineage_length_all{gi} = [];
    
    Nlineages = 0;
    
    %% Process each file:
    for fi = 1:numel(files)
        
        % Load file:
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        % For each chamber, plot the mother cell fluorescence, growth rate & divisions:
        for chamber = 1:numel(res)
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
           
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames:
            length_mother = res(chamber).lineage(1).length; % Measured length over those frames
            growthrate_mother = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,1); % Growth rate
            divisions_mother = res(chamber).lineage(1).daughters > 0; % Identify when divisions happen
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                fluo_mother = fluo_mother(frames_mother);
                length_mother = length_mother(frames_mother);
                growthrate_mother = growthrate_mother(frames_mother);
                divisions_mother = divisions_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
            
            if numel(frames_mother) < 3 % Only work with data that has at least 3 points
                continue
            end
            
            % Smooth mother cell fluorescence
            fluo_mother = smooth(fluo_mother, swindow);
            
            % Crop data so that it's the same length as the derivative data
            growthrate_mother = growthrate_mother(2:end);
            growthrate_mother = smooth(growthrate_mother, swindow);
            fluo_mother = fluo_mother(2:end);
            length_mother = length_mother(1:(end-1));
            time = frames_mother(2:end)*min_per_frame;
            
            if mean(growthrate_mother) < 0.5 % Quality control - don't work with cells that aren't growing
                continue
            end
            
            if isempty(fluo_mother) || length(fluo_mother) < 3 % findpeaks needs at least 3 data points
                continue
            end

%             findpeaks(fluo_mother/mean(fluo_mother), time, 'MinPeakProminence', 0.15, 'Annotate', 'extent'); ylim([0.5 1.5]); pause

%             if (length(fluo_mother) == Nframes-1 && ~any(growthrate_mother < -3)) % Quality check: mother exists in all frames and never has very negative growth
                [pks, loc, widths, prominence] = findpeaks(fluo_mother/mean(fluo_mother), time, 'MinPeakProminence', 0.5);
                if isempty(pks)
                    peaks_per_hour = [peaks_per_hour 0];
                    peak_duration = [peak_duration 0];
                    peak_amplitude = [peak_amplitude 0];
                else
                    peaks_per_hour = [peaks_per_hour numel(pks)/((time(end)-time(1))/60)];
 
%                     %% This commented code is a variant that only considers the time between pulses when doing this calculation
%                     if numel(pks) < 2 % only one peak, can't find the amount of time between the peaks
%                         peaks_per_hour = [peaks_per_hour 0];
%                     else
%                         peaks_per_hour = [peaks_per_hour 60./diff(loc)]; % find number of peaks per hour, only consider the times between two measured peaks (not the start and end of the movie)
%                     end
                    peak_duration = [peak_duration widths];
                    peak_amplitude = [peak_amplitude prominence'];
                end

                fluo_mother_all = [fluo_mother_all; fluo_mother];
                length_mother_all = [length_mother_all; length_mother'];
                growthrate_mother_all = [growthrate_mother_all; growthrate_mother];
                
                lineage_length_all{gi} = [lineage_length_all{gi} time(end)-time(1)];
                
                if numel(fluo_mother) >= 15*60/min_per_frame % record 15 hours of data in a matrix so that you can look at means over time
                    fluo_mother_all_matrix = [fluo_mother_all_matrix; fluo_mother(1:15*60/min_per_frame)'];
                end
                
                Nlineages = Nlineages + 1;
                
%             end
        end
        
    end
    
    if isempty(fluo_mother_all)
        continue
    end
    
    disp(['*** ', gene_list{gi}, ', # of lineages: ', num2str(Nlineages)])
    
    figure(1)
    [y, x] = ksdensity(fluo_mother_all, linspace(500, 8000, 400));
    plot(x, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    fluo_all_x{gi} = x;
    fluo_all_y{gi} = y;
    title('Fluo')
    xlim([500 8000])
    xlabel('Fluorescence (A. U.)')
    
    figure(2)
    [y, x] = ksdensity(length_mother_all, linspace(0, 15/um_per_pixel, 400));
    plot(x*um_per_pixel, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    length_all_x{gi} = x;
    length_all_y{gi} = y;
    title('Length')
    xlim([0 15])
    xlabel('Length (\mu m)')
    
    figure(3)
    [y, x] = ksdensity(growthrate_mother_all, linspace(-0.05, 2.5, 400));
    plot(x, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    growthrate_all_x{gi} = x;
    growthrate_all_y{gi} = y;
    title('Growth Rate')
    xlim([-0.05 2.5])
    xlabel('Growth Rate (1/h)')

    if isempty(peaks_per_hour)
        continue
    end
    
    figure(4)
    [y, x] = ksdensity(peaks_per_hour, linspace(0, 1, 400), 'Bandwidth', 0.03); %, 'Bandwidth', 0.025);
    plot(x, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    pulse_freq_all_x{gi} = x;
    pulse_freq_all_y{gi} = y;
    title('Peaks per hour')
    xlim([0 1])
    
    figure(5)
    [y, x] = ksdensity(peak_duration/60, linspace(0, 6, 400));
    plot(x, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    pulse_duration_all_x{gi} = x;
    pulse_duration_all_y{gi} = y;
    title('Peak duration (hours)')
    xlim([0 6])
    
    figure(6)
    [y, x] = ksdensity(peak_amplitude, linspace(0, 2.5, 400));
    plot(x, y/max(y) + offset*(numel(gene_list)-gi), 'Color', clist(gi,:)); hold on
    pulse_amp_all_x{gi} = x;
    pulse_amp_all_y{gi} = y;
    title('Peak amplitude (of fluo/mean(fluo))')
    xlim([0 2.2])
    
    peaks_per_hour_all{gi} = peaks_per_hour;
    peak_duration_all{gi} = peak_duration/60;
    peak_amplitude_all{gi} = peak_amplitude;

    figure(7)
    subplot(4,4,gi); dscatter(length_mother_all*um_per_pixel, fluo_mother_all/mean(fluo_mother_all)); 
    axis([0 20 0 6])
    R = corrcoef(length_mother_all, fluo_mother_all);
    title([gene_list{gi}, ', ', num2str(R(1,2))])
    
    figure(9)
    subplot(2,1,1); plot(gi, R(1,2), 'bo'); hold on
    
    figure(8)
    subplot(4,4,gi); dscatter(growthrate_mother_all, fluo_mother_all/mean(fluo_mother_all));
    axis([-0.05 0.2 0 6])
    R = corrcoef(growthrate_mother_all, fluo_mother_all);
    title([gene_list{gi}, ', ', num2str(R(1,2))])
    
    figure(10)
    t = (0:15*60/min_per_frame - 1)*5/60;
    m = mean(fluo_mother_all_matrix/mean(mean(fluo_mother_all_matrix)));
    s = std(fluo_mother_all_matrix/mean(mean(fluo_mother_all_matrix)));
%     subplot(4,4,gi); errorbar(t(1:5:end), m(1:5:end), s(1:5:end))
%     subplot(4,4,gi); shadedErrorBar(t, m, s)
%     subplot(4,4,gi); plot(t, fluo_mother_all_matrix/mean(mean(fluo_mother_all_matrix))); hold on
    subplot(4,4,gi); shadedErrorBar(t, m, abs(m - prctile(fluo_mother_all_matrix/mean(mean(fluo_mother_all_matrix)), [25 75])))
    title(gene_list{gi})
    axis([0 15 0 3])
    set(gca, 'XTick', 0:5:15)
    
    clear frame_range
end

save pulse_metrics_data gene_list pulse_freq_all_x pulse_freq_all_y pulse_duration_all_x pulse_duration_all_y pulse_amp_all_x pulse_amp_all_y ...
    peaks_per_hour_all peak_duration_all peak_amplitude_all lineage_length_all fluo_all_x fluo_all_y growthrate_all_x growthrate_all_y length_all_x length_all_y

if offset
    for i = 1:6
        figure(i)
        ylim([0 numel(gene_list)])
        set(gca, 'YTick', 0.5+(0:numel(gene_list)-1), 'YTickLabel', fliplr(gene_list))
    end
end

figure(9)
set(gca, 'XTick', 1:numel(gene_list), 'XTickLabel', gene_list)
xlim([0 numel(gene_list)+1])
ylim([-0.5 1])
ylabel('Correlation Coefficient (Cell length - Fluorescence)')
