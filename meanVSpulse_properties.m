clear all, close all

load pulse_metrics_data_all

snap_mean_exp_normalized = [
61.7688; % araC, mean = 61.7688, CV^2 = 0.023591
33.8125; % bolA, mean = 33.8125, CV^2 = 0.050149
16.429; % dinB, mean = 16.429, CV^2 = 0.027472
548.5076; % evgA, mean = 548.5076, CV^2 = 0.04025
265.4158; % gadW, mean = 265.4158, CV^2 = 0.071181
996.7878; % gadX, mean = 996.7878, CV^2 = 0.070644
104.7162; % marA, mean = 104.7162, CV^2 = 0.058048
47.943; % metJ, mean = 47.943, CV^2 = 0.023304
54.9615; % oxyR, mean = 54.9615, CV^2 = 0.027835
44.1549; % phoP, mean = 44.1549, CV^2 = 0.01807
413.6904; % purA, mean = 413.6904, CV^2 = 0.070002
178.7133; % recA, mean = 178.7133, CV^2 = 0.11665
247.2407; % rob, mean = 247.2407, CV^2 = 0.049853
232.0105; % rpoH, mean = 232.0105, CV^2 = 0.049186
17.9108; % sulA, mean = 17.9108, CV^2 = 0.031476
% without_promoter, mean = 7.8804, CV^2 = 0.0024199
];

for i = 1:15
    subplot(2,2,1); plot(snap_mean_exp_normalized(i), mean(peaks_per_hour_all{i}), 'b.', 'MarkerSize', 10); hold on; xlabel('Mean GFP (a.u.)'); ylabel('Mean peaks per hour (1/h)')
    subplot(2,2,2); plot(snap_mean_exp_normalized(i), mean(peak_duration_all{i}), 'b.', 'MarkerSize', 10); hold on; xlabel('Mean GFP (a.u.)'); ylabel('Mean duration (h)')
    subplot(2,2,3); plot(snap_mean_exp_normalized(i), mean(peak_amplitude_all{i}), 'b.', 'MarkerSize', 10); hold on; xlabel('Mean GFP (a.u.)'); ylabel('Mean amplitude (mean normalized)')
end