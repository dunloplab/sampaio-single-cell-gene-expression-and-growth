close all, clear all

figure
hold on

%Part 1: Get data from MM experiments
basepath = '../mother_machine/data/'; % Directory where is data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets

swindow = 5;

for gi = 1:numel(gene_list) %Cycle through all genes for separate cor. coeffs
    
    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    all_pulses = [];

    
    for fi = 1:numel(files)
        all_fluos = [];
        all_grs = [];
        all_lengths = [];
        
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        for chamber = 1:numel(res)
            
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
            
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames:
            length_mother = res(chamber).lineage(1).length; % Measured length over those frames
            growthrate_mother = compute_growthrate(res(chamber).lineage,1); % Growth rate as a length diff (quick and dirty implementation)
            divisions_mother = res(chamber).lineage(1).daughters > 0; % Identify when divisions happen
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                fluo_mother = fluo_mother(frames_mother);
                length_mother = length_mother(frames_mother);
                growthrate_mother = growthrate_mother(frames_mother);
                divisions_mother = divisions_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
    
            if numel(frames_mother) < 3 %exclude data with fewer than 3 data points
                continue
            end
            
            all_fluos = [all_fluos fluo_mother];
            all_grs = [all_grs growthrate_mother(2:end)];
            all_lengths = [all_lengths length_mother];
            
            fluo_mother = smooth(fluo_mother,5);
            [peaks, loc, widths, prominence] = findpeaks(fluo_mother, frames_mother, 'MinPeakProminence', mean(fluo_mother)*0.1);
            prominence
            all_pulses = [all_pulses prominence'/mean(fluo_mother)];
            
        end  
%         figure(1)
%         hold on
%         subplot(4, 4, gi)
%         title(gene_list{gi})
%         hold on
%         [N,edges] = histcounts(all_fluos, 0:100:2500, 'Normalization', 'probability');
%         edges = edges(2:end) - (edges(2)-edges(1))/2;
%         plot(edges, N, 'LineWidth', 1);
%         xlabel("fluorescence (A.U.)")
%         ylabel("probability")
%         
%         figure(2)
%         hold on
%         subplot(4, 4, gi)
%         title(gene_list{gi})
%         hold on
%         [N,edges] = histcounts(all_grs, -10:1:15, 'Normalization', 'probability');
%         edges = edges(2:end) - (edges(2)-edges(1))/2;
%         plot(edges, N, 'LineWidth', 1);
%         xlabel("growth rate (pixels/frame)")
%         ylabel("probability")
%         
%         figure(3)
%         hold on
%         subplot(4, 4, gi)
%         title(gene_list{gi})
%         hold on
%         [N,edges] = histcounts(all_areas, 10:20:420, 'Normalization', 'probability');
%         edges = edges(2:end) - (edges(2)-edges(1))/2;
%         plot(edges, N, 'LineWidth', 1);
%         xlabel("cell area (pixels)")
%         ylabel("probability")
%         
%         figure(4)
%         hold on
%         subplot(4, 4, gi)
%         title(gene_list{gi})
%         hold on
%         [N,edges] = histcounts(all_lengths, 0:3:80, 'Normalization', 'probability');
%         edges = edges(2:end) - (edges(2)-edges(1))/2;
%         plot(edges, N, 'LineWidth', 1);
%         xlabel("cell length (pixels)")
%         ylabel("probability")
        
    end
        %Plot promiance by gene only, since there is not enough data per
        %replicate
        figure(5)
        hold on
        subplot(4, 4, gi)
        title(gene_list{gi})
        hold on
        line([0.15 0.15],[0 0.5])
        [N,edges] = histcounts(all_pulses, 0:0.025:1, 'Normalization', 'probability');
        edges = edges(2:end) - (edges(2)-edges(1))/2;
        plot(edges, N, 'LineWidth', 1);
        xlabel("pulse prominence / mean")
        ylabel("probability")
        ylim([0 0.5])
        
        clear frame_range
end

