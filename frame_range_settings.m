function frame_range = frame_range_settings(gene, basepath)

frame_range = NaN; % initialize to NaN to flag cases where it doesn't get set

% Note: these need to be contiguous frames.
if strcmp(basepath(end-4:end), 'data/')
    switch gene
        case {'metJ', 'oxyR', 'phoP'}
            frame_range = 1:188; % after this is conditioned media
        case 'araC'
            frame_range = 1:182; % for the 20190911 data after this is conditioned media (actually after 188); for the 20200302 data after this cipro is added
        case 'bolA'
            frame_range = 24:289; % crop off the start of the movie b/c fluor values are higher there
        case 'gadX'
            frame_range = 24:205; % crop off the start of the movie b/c fluor values are higher there; data is from two dates so this is the shorter of the two
        case 'purA'
            frame_range = 24:241; % crop off the start of the movie b/c fluor values are higher there
        case 'zzz-W6'
            frame_range = 10:203; % cipro is added after this point, cut off the start, but leave ~15h of data
    end
elseif strcmp(basepath(end-7:end), 'data_CM/')
    switch gene
        case {'bolA_CM02', 'bolA_CM06', 'bolA_CM12'}
            frame_range = 24:209; % see NS note on GSheet about before and after
        case {'metJ', 'araC', 'oxyR', 'phoP'}
            frame_range = 190:289;
        case {'recA', 'dinB', 'sulA'}
            frame_range = 24:261;
    end
elseif strcmp(basepath(end-15:end), 'data_delta_rpoS/')
    frame_range = 1:199;
elseif strcmp(basepath(end-17:end), 'data_constitutive/')
    switch gene_list{gi}
        case {'W6'}
            frame_range = 16:197;
    end
end

end