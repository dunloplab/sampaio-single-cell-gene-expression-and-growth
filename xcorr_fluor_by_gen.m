%Load files
close all; clear all

basepath = '../mother_machine/data/'; % Directory where is data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets

min_per_frame = 5;

figure
hold on

for gi = 1:numel(gene_list) %Cycle through all genes for separate cor. coeffs
    
%Initialize cells to hold the xcor and lag values.     
all_mother_daughter_xs = {};
all_lags_motherdaughter = {};

all_mother_gdaughter_xs = {};    
all_lags_mother_gdaughter = {};

all_mother_ggdaughter_xs = {};
all_lags_mother_ggdaughter = {};

%Initialize cell to hold correlation coefficient values for mean GFP
corCoeffData = {[] [] []};

    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.    
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    
    for fi = 1:numel(files)
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        for chamber = 1:numel(res)
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end

            mothernbs = zeros(1, numel(res(chamber).lineage)); %Collect mother numbers in one array to make recursive functions easier to handle
            for cell = 1:numel(res(chamber).lineage)
                mothernbs(cell) = res(chamber).lineage(cell).mothernb;
            end
            
            for cell = 1:numel(res(chamber).lineage)
                    genNum = getGenNumber(cell, mothernbs); %Find if cell is daughter, granddaughter etc
                    urMutter = findUrMutter(cell, mothernbs); %Finds oldest ancestor to exclude cells descended from cells other than #1
                    
                    if (genNum >0 && genNum < 4 && urMutter == 1) %Ignores great-great-granddaughters and cells not descended from the mother
                        
                        frame = res(chamber).lineage(cell).framenbs(1); 
                        
                        if frame > numel(res(chamber).lineage(1).fluo1) %Catches instances where mother cell is lost while daughter is present. Rare but happens
                            continue
                        end
                        
                        [a b c d] = findIndices(1, cell,  res(chamber), genNum); %Window the data appropriately to interdivision time of each cell 
                        
                        %Ignore the cell if any part of its lifetime is outside of the cropped frame range
                        if exist('frame_range') & (res(chamber).lineage(1).framenbs(a) < frame_range(1) | res(chamber).lineage(cell).framenbs(d) > frame_range(end))    
                            continue 
                        end 
                        
                        branch_frames = (res(chamber).lineage(cell).framenbs(c:d)); %Finds the frame numbers corresponding to the descendant's interdivision time

                        if branch_frames(end) > numel(res(chamber).lineage(1).fluo1) %Skip if mother cell is untracked during this range
                            continue
                        end
                          
                        mother_branch = res(chamber).lineage(1).fluo1(branch_frames);
                        daughter_branch = res(chamber).lineage(cell).fluo1(c:d);
                        
                        if numel(branch_frames) < 3 %Only compute cross_co for at least 3 data points
                            continue
                        end

                        %Compute cross corr
                        A = mother_branch;
                        B = daughter_branch;
                        autox = xcov(A, 'unbiased');
                        autoy = xcov(B, 'unbiased');
                        autox0 = autox(length(A));
                        autoy0 = autoy(length(B));
                        crossxy = xcov(A, B, 'unbiased');
                        crossxy = crossxy/sqrt(autox0*autoy0); % Normalize by zero-lag values
                        
                        lags = min_per_frame*[-(numel(crossxy)-1)/2:(numel(crossxy)-1)/2];

                        %Store in the appropriate cell
                        if genNum == 1 %Daughters
                            
                            all_mother_daughter_xs{end+1} = crossxy;
                            all_lags_motherdaughter{end+1} = lags; 
                            
                        elseif genNum == 2 %Granddaughters

                            all_mother_gdaughter_xs{end+1} = crossxy;
                            all_lags_mother_gdaughter{end+1} = lags;
                            
                        elseif genNum == 3 %Great-Granddaughters

                            all_mother_ggdaughter_xs{end+1} = crossxy;
                            all_lags_mother_ggdaughter{end+1} = lags;  
                            
                        end
                        
                        %Add mean fluorescence values to the vector for
                        %computing correlation coefficient
                        %Mother cell indices: a:b compares the mother "stem" to
                        %the descendant branch, while c:d compares both
                        %"branches" over the same time frame.
                        corCoeffData{genNum} =[corCoeffData{genNum} [mean(res(chamber).lineage(cell).fluo1(c:d)); mean(res(chamber).lineage(1).fluo1(a:b))]];
                    end
            end
        end 
    end
    
    figure(1)
    hold on
    
    subplot(4, 4, gi)
    hold on
    
    %Window for the lag values of the data, in minutes
    leftbound = -60;
    rightbound = 60;
    
    %Use helper function to window the data, convert to matrix, take the
    %mean, and plot
    mother_daughter_xs =  Cell2PaddedMat(all_mother_daughter_xs, all_lags_motherdaughter, -60, 60);
    plot(leftbound:5:rightbound, nanmean(mother_daughter_xs), 'Color', [0 0 1],'LineWidth', 1.5);
    
    mother_gdaughter_xs = Cell2PaddedMat(all_mother_gdaughter_xs, all_lags_mother_gdaughter, -60, 60);
    plot(leftbound:5:rightbound, nanmean(mother_gdaughter_xs), 'Color',[0.1 0.8 0.88], 'LineWidth',1.5);
    
    mother_ggdaughter_xs = Cell2PaddedMat(all_mother_ggdaughter_xs, all_lags_mother_ggdaughter, -60, 60);
    plot(leftbound:5:rightbound, nanmean(mother_ggdaughter_xs), 'Color',[0.1 0.96 0.3], 'LineWidth',1.5); 
    
    title(gene_list{gi})
    
    ylim([-1 1])
    
    figure(2)
    hold on
    
    subplot(3, 5, gi)
    hold on
    
    corCoeffs = [];
    for gens = 1:3
        if isempty(corCoeffData{gens})
            continue
        end
        disp(['Number of cells compared. Generation = ', num2str(gens), ', # = ', num2str(numel(corCoeffData{gens}(1,:)))])
        cc = corrcoef(corCoeffData{gens}(1,:), corCoeffData{gens}(2, :));
        corCoeffs = [corCoeffs cc(1,2)];
    end
    
     bar(corCoeffs); 
     title(gene_list(gi))
     ylim([0 1]);
     set(gca, 'XTick', 1:3)
     
     clear frame_range
    
end


%%%%
%%%Helper functions%%%
%%%%

%Converts a cell to a matrix that's padded with NaN values for any rows
%that are shorter than the full-sized matrix. 
%Inputs:Leftbound and rightbound are the time lag values (not the cell
%indices) to crop the data at
%CellA is the xcorr data to be formatted
%CellB contains the lag values corresponding to each index in CellA
function paddedMat = Cell2PaddedMat(cellA, cellB, leftbound, rightbound)
    paddedMat = NaN(size(cellA,2), (rightbound-leftbound)/5 + 1);
    for i = 1:size(cellA,2)
       if numel(cellA{i})> size(paddedMat, 2)
           index1 = find(cellB{i} == leftbound);
           index2 = find(cellB{i} == rightbound);
           paddedMat(i, :) = cellA{i}(index1:index2);
       else
           index1 = (cellB{i}(1)-leftbound)/5 + 1;
           index2 = index1 + numel(cellA{i}) - 1;
           paddedMat(i,index1:index2) = cellA{i};
       end
    end

end

%Recursive function to find generation number of indicated cell given
%mother numbers. Gen 0 is a mother cell, 1 is daughter, 2 is granddaughter
%etc

function genNumber = getGenNumber(cell, mothernbs)
if mothernbs(cell) == 0 
   genNumber = 0;
   
else
   genNumber = getGenNumber(mothernbs(cell), mothernbs) + 1;
end

end

%Recursive function to find the original mother / earliest known ancestor of a cell
%(German has a word for this, "Urmutter")

function urMutter = findUrMutter(cell, mothernbs)
if mothernbs(cell) == 0 
   urMutter = cell;
   
else
   urMutter = findUrMutter(mothernbs(cell), mothernbs);
end

end

%Find indice bounds for fluorescence values used in mean calculation that
%is then fed to the correlation coefficient. Mother cell fluorescence is
%a:b, daughter cell is c:d. Note that these are the indices to be used in
%the fluorescence vectors, NOT frame numbers.
function [a b c d] = findIndices(mother, daughter, chamber, genNum)
c = 1;
d = find(chamber.lineage(daughter).daughters ~= 0);
if isempty(d)
    d = numel(chamber.lineage(daughter).daughters);
end

if genNum == 1
    b = find(chamber.lineage(mother).daughters == daughter);
elseif genNum == 2
    b = find(chamber.lineage(mother).daughters == chamber.lineage(daughter).mothernb);
else 
    b = find(chamber.lineage(mother).daughters == chamber.lineage( chamber.lineage(daughter).mothernb ).mothernb);
end

if b < 1
    b = b-1;
end

division_frames = find(chamber.lineage(mother).daughters ~= 0);
division_frame_b_index = find(division_frames == b);

if (division_frame_b_index == 1)
    a = 1;
else
a = division_frames(division_frame_b_index - 1);
end

end


