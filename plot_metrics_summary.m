close all; clear all

% % ------------- Compare thresholds ------------
% thresh_datasets{1} = 'pulse_metrics_data0p3';
% thresh_datasets{2} = 'pulse_metrics_data0p4';
% thresh_datasets{3} = 'pulse_metrics_data0p5';
% thresh_datasets{4} = 'pulse_metrics_data0p6';
% thresh_datasets{5} = 'pulse_metrics_data0p7';
% clist = [linspace(0, 0, numel(thresh_datasets))' linspace(0, 1, numel(thresh_datasets))' linspace(1, 0, numel(thresh_datasets))'];


% ------------- Standard Plots ------------
thresh_datasets{1} = 'pulse_metrics_data_all';
clist = [linspace(0, 0, numel(thresh_datasets))' linspace(0, 1, numel(thresh_datasets))' linspace(1, 0, numel(thresh_datasets))'];


offset = 1; % set to 0 to have plots of top of each other, 1 for spaced out plots
filled_plots = 0; % set to 0 for just a line showing top of histogram, 1 is filled histograms

% Set up the legend in the right order
figure(10)
for ti = 1:numel(thresh_datasets)
    plot([0 0], [0 0], 'Color', clist(ti,:)); hold on
end

first_iteration = 1;
for ti = 1:numel(thresh_datasets)
    load(thresh_datasets{ti})
    
    for gi = 1:numel(gene_list)
        figure(8); hold on; title('Freq')
        if ~isempty(pulse_freq_all_y{gi})
            if filled_plots
                bar(pulse_freq_all_x{gi}, pulse_freq_all_y{gi}/max(pulse_freq_all_y{gi}) + offset*(numel(gene_list)-gi), 'FaceColor', clist(ti,:));
                rectangle('Position', [0 0 1 offset*(numel(gene_list)-gi)], 'FaceColor', 'w', 'LineWidth', 1, 'EdgeColor', 'w') % [x y w h]
            else
                plot(pulse_freq_all_x{gi}, pulse_freq_all_y{gi}/max(pulse_freq_all_y{gi}) + offset*(numel(gene_list)-gi), 'Color', clist(ti,:));
            end
        end
        plot([0 1], [0 0] + offset*(numel(gene_list)-gi), 'Color', [0.1 0.1 0.1])
        
        if first_iteration % only plot this box the first time you loop through
            L = lineage_length_all{gi};
%             rectangle('Position', [0 offset*(numel(gene_list)-gi) 1/(median(L)/60) 1], 'FaceColor', [0 0 0 0.25], 'LineStyle', 'none') % [x y w h], color is [r g b alpha]
        end
        
        disp([gene_list{gi}, ' median = ', num2str(median(L)/60, 3), ', max = ', num2str(max(L)/60, 3)])
        
        fr = numel(find(peaks_per_hour_all{gi} ~= 0))/numel(peaks_per_hour_all{gi}); % fraction of traces that have at least 1 pulse
        
        figure(9); hold on; title('Duration')
        if ~isempty(pulse_duration_all_y{gi})
            if filled_plots
                bar(pulse_duration_all_x{gi}, fr*pulse_duration_all_y{gi}/max(pulse_duration_all_y{gi}) + offset*(numel(gene_list)-gi), 'FaceColor', clist(ti,:));
                rectangle('Position', [0 0 6 offset*(numel(gene_list)-gi)], 'FaceColor', 'w', 'LineWidth', 1, 'EdgeColor', 'w') % [x y w h]
            else
                plot(pulse_duration_all_x{gi}, fr*pulse_duration_all_y{gi}/max(pulse_duration_all_y{gi}) + offset*(numel(gene_list)-gi), 'Color', clist(ti,:))
            end
        end
        plot([0 6], [0 0] + offset*(numel(gene_list)-gi), 'Color', [0.1 0.1 0.1])
        
        figure(10); hold on; title('Amp')
        if ~isempty(pulse_amp_all_y{gi})
            if filled_plots
                bar(pulse_amp_all_x{gi}, fr*pulse_amp_all_y{gi}/max(pulse_amp_all_y{gi}) + offset*(numel(gene_list)-gi), 'FaceColor', clist(ti,:)); 
                rectangle('Position', [0 0 2.5 offset*(numel(gene_list)-gi)], 'FaceColor', 'w', 'LineWidth', 1, 'EdgeColor', 'w') % [x y w h]
            else
                plot(pulse_amp_all_x{gi}, fr*pulse_amp_all_y{gi}/max(pulse_amp_all_y{gi}) + offset*(numel(gene_list)-gi), 'Color', clist(ti,:))
            end
        end
        plot([0 2.5], [0 0] + offset*(numel(gene_list)-gi), 'Color', [0.1 0.1 0.1])
        
    end
    first_iteration = 0;
end

figure(8); set(gca, 'YTick', 0.5+(0:numel(gene_list)-1), 'YTickLabel', fliplr(gene_list)); xlim([0 0.4])
figure(9); set(gca, 'YTick', 0.5+(0:numel(gene_list)-1), 'YTickLabel', fliplr(gene_list)); xlim([0 6])
figure(10); set(gca, 'YTick', 0.5+(0:numel(gene_list)-1), 'YTickLabel', fliplr(gene_list)); xlim([0 2])
legend('0.3', '0.4', '0.5', '0.6', '0.7')
