clear all, close all

widthX = 80;
widthY = 80;

dir_start = '../snapshots/TIFFs/';

dir_end{1} = 'Promoterless_800ms';      means(1) = 9000;    pos(1) = 2;     startYX{1} = [215 380];     titles{1} = 'None';
dir_end{2} = 'PfecI_gfp_triplicate';    means(2) = 8243;    pos(2) = 2;     startYX{2} = [220 300];     titles{2} = 'fecI';
dir_end{3} = '20200220_acrAB';          means(3) = 5337;    pos(3) = 6;     startYX{3} = [440 305];     titles{3} = 'acrAB';
dir_end{4} = 'PphoP_gfp_triplicates';   means(4) = 4415;    pos(4) = 7;     startYX{4} = [260 60];      titles{4} = 'phoP';
dir_end{5} = 'PcysB_gfp_triplicates';   means(5) = 4751;    pos(5) = 7;     startYX{5} = [270 320];     titles{5} = 'cysB';
dir_end{6} = 'PompR_gfp_triplicates';   means(6) = 5005;    pos(6) = 6;     startYX{6} = [360 460];     titles{6} = 'ompR';
dir_end{7} = 'PfhlA_gfp_triplicate';    means(7) = 5149;    pos(7) = 9;     startYX{7} = [385 275];     titles{7} = 'flhA';
dir_end{8} = 'PdksA_gfp_triplicate';    means(8) = 5288;    pos(8) = 3;     startYX{8} = [100 225];     titles{8} = 'dksA';
dir_end{9} = 'PmetJ_gfp_triplicates';   means(9) = 4794;    pos(9) = 9;     startYX{9} = [225 305];     titles{9} = 'metJ';
dir_end{10} = 'ParaC_gfp_triplicates';  means(10) = 6177;   pos(10) = 2;    startYX{10} = [222 435];    titles{10} = 'araC';
dir_end{11} = 'Pfur_gfp_triplicates';   means(11) = 6022;   pos(11) = 1;    startYX{11} = [350 320];    titles{11} = 'fur';
dir_end{12} = 'PydeO_gfp_triplicates';  means(12) = 6072;   pos(12) = 1;    startYX{12} = [140 300];    titles{12} = 'ydeO';
dir_end{13} = 'PihfB_gfp_triplicates';  means(13) = 7439;   pos(13) = 9;    startYX{13} = [390 180];    titles{13} = 'ihfB';
dir_end{14} = 'dinB_300ms';             means(14) = 4929;   pos(14) = 2;    startYX{14} = [300 255];    titles{14} = 'dinB';
dir_end{15} = 'PoxyR_gfp_triplicates';  means(15) = 5496;   pos(15) = 7;    startYX{15} = [395 120];    titles{15} = 'oxyR';
dir_end{16} = 'Pcrp_gfp_triplicates';   means(16) = 5829;   pos(16) = 2;    startYX{16} = [345 365];    titles{16} = 'crp';
dir_end{17} = 'PslyA_gfp_triplicates';  means(17) = 5777;   pos(17) = 7;    startYX{17} = [240 130];    titles{17} = 'slyA';
dir_end{18} = '20200220_sulA';          means(18) = 5373;   pos(18) = 3;    startYX{18} = [165 205];    titles{18} = 'sulA';
dir_end{19} = 'PevgA_gfp_triplicates';  means(19) = 5485;   pos(19) = 1;    startYX{19} = [295 105];    titles{19} = 'evgA';
dir_end{20} = 'PrpoE_gfp_triplicates';  means(20) = 5929;   pos(20) = 1;    startYX{20} = [370 205];    titles{20} = 'rpoE';
dir_end{21} = '20200220_purA';          means(21) = 20680;  pos(21) = 3;    startYX{21} = [280 135];    titles{21} = 'purA';
dir_end{22} = 'PgadX_gfp_triplicates';  means(22) = 9968;   pos(22) = 6;    startYX{22} = [130 55];     titles{22} = 'gadX';
dir_end{23} = 'PgadW_gfp_triplicates';  means(23) = 5308;   pos(23) = 4;    startYX{23} = [250 260];    titles{23} = 'gadW';
dir_end{24} = '20200220_recA';          means(24) = 17870;  pos(24) = 3;    startYX{24} = [250 393];    titles{24} = 'recA'; %[175 30];
dir_end{25} = 'PbolA_gfp_3h20m';        means(25) = 6762;   pos(25) = 9;    startYX{25} = [345 315];    titles{25} = 'bolA';
dir_end{26} = 'Prob_gfp_triplicates';   means(26) = 7417;   pos(26) = 1;    startYX{26} = [0 0];
dir_end{27} = '20200220_marA';          means(27) = 5236;   pos(27) = 3;    startYX{27} = [0 0];
dir_end{28} = 'PrpoH_gfp_triplicates';  means(28) = 6960;   pos(28) = 5;    startYX{28} = [0 0];

plot_i = 1;

for ic = [1 10 22 24]
    
    fullframe = [1 520 1 696];
   
    box(ic, :) = [startYX{ic}(1) startYX{ic}(1)+widthY startYX{ic}(2) startYX{ic}(2)+widthX];    
%     box(ic, :) = fullframe;

    imP = imread([dir_start dir_end{ic}, 'XY', num2str(pos(ic)), 'C', '1.tif']);
    imG = imread([dir_start dir_end{ic}, 'XY', num2str(pos(ic)), 'C', '2.tif']);
    
% [~, position] = imcrop(imP);
% position = round(position);
% box = [position(2) position(2)+position(4)-1 position(1) position(1)+position(3)-1]

    imP = double(imP);
    imG = double(imG);
    
    pbound = [mean(mean(imP))*0.5 mean(mean(imP))*1.5];
    gbound = [means(ic)*0.55 means(ic)*2];
    
    imP = imP(box(ic,1):box(ic,2), box(ic,3):box(ic,4));
    imG = imG(box(ic,1):box(ic,2), box(ic,3):box(ic,4));
% imP = imP(box(1):box(2), box(3):box(4));
% imG = imG(box(1):box(2), box(3):box(4));


    imP(imP < pbound(1)) = pbound(1);
    imG(imG < gbound(1)) = gbound(1);
    imP(imP > pbound(2)) = pbound(2);
    imG(imG > gbound(2)) = gbound(2);
    
    imP = (imP-pbound(1))/(pbound(2)-pbound(1));
    imG = (imG-gbound(1))/(gbound(2)-gbound(1));
    
    phase = makergb(imP, imP, imP, 0); % the final zero indicates that it should not be normalized (we've already normalized here)
    rgb = makergb(zeros(size(imP)).*imP, imG, zeros(size(imP)).*imP, 0);
    snap = [phase rgb];
    
    um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera
    scale_bar_size = round(2/um_per_pixel); % 2um in pixels
    snap(end-6:end-3, 3+(1:scale_bar_size), :) = 256; % White bar
    
    subplot(5, 5, plot_i)
    imshow(snap)
    title(titles{ic})
    
    plot_i = plot_i + 1;
end

% % % Cropping -- comment out box crop part above and you can use this to look at the full image and then crop yourself
% % dir_end{ic}
% [~, position] = imcrop(snap);
% position = round(position);
% box = [position(2) position(2)+position(4)-1 position(1) position(1)+position(3)-1]

