close all; clear all

basepath = '../mother_machine/data/'; % Directory where is data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets
% gene_list{1} = 'araC';

min_per_frame = 5;

all_means = [];
all_stds = [];

for gi = 1:numel(gene_list) % Cycle through all genes
    
    %Find window limits for data, to crop out data that falls outside of the
    %window of interest
    
    div_means = [];
    
    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    
    for fi = 1:numel(files)

        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])

        for chamber = 1:numel(res)
            
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
            
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            divisions_mother = res(chamber).lineage(1).daughters;
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                divisions_mother = divisions_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
            
            if numel(frames_mother) < Nframes % Discard mother cells that are not present for the full movie
                continue
            end
       
            div_mean = div_time(divisions_mother, min_per_frame);
            
            div_means = [div_means div_mean];            
        end  

    end
    
    if ~isempty(div_means(div_means >= 500))
        disp(['Note: excluding [', num2str(numel(div_means(div_means >= 500))), '] unrealistically long division times from this data set'])
        div_means = div_means(div_means < 500); % exclude any data points that are unrealistically long
    end
    
    all_means = [all_means nanmean(div_means)];
    all_stds = [all_stds nanstd(div_means)];
    all_div_means{gi} = div_means;
    
    clear frame_range
end

%% Plot
% gene_list2 = {gene_list{1:8}, gene_list{10:16}};
% X = categorical(gene_list2);

bar(categorical(gene_list), all_means);
hold on

er = errorbar(categorical(gene_list), all_means, all_stds, all_stds);
er.Color = [0 0 0];                            
er.LineStyle = 'none';  

ylabel('mean mother cell division time (min)');

save division_time_data gene_list all_means all_stds all_div_means


%%%SUBFUNCTIONS%%%

%Calculates the average division time of the cell, in minutes
%INPUTS
%divisions: Vector of each cell division;  min_per_frame minutes per frame
%of movie data
%OUTPUTS
%aaverageDivTime: average time in minutes for each division
function averageDivTime = div_time(divisions, min_per_frame)
    div_times = [];

    div_indices = find(divisions > 0);
    
    if numel(div_indices)<2
        averageDivTime = NaN;
    else
        for i = flip(2:numel(div_indices))
            div = div_indices(i)-div_indices(i-1);
            div_times = [div_times div];
        end
    
        averageDivTime = min_per_frame*nanmean(div_times);
    end
end