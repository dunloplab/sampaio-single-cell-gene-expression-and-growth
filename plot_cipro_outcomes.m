close all; clear all;

basepath = '../mother_machine/data_cipro/outcomes/'; % Directory where is data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

normalization = true;

% gene_list{1} = 'W6_1';
% gene_list{2} = 'W6_2';

% gene_list{1} = 'bolA';

% gene_list{1} = 'araC';

% gene_list{1} = 'recA1';
% gene_list{2} = 'recA2';

gene_list{1} = 'gadX2';
gene_list{2} = 'gadX3';
gene_list{3} = 'gadX4';

min_per_frame = 5;
swindow = 5; % smoothing window
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera
averaging_window = 0; % Number of frames pre-AB addition for determining average fluorescence

a_fluo_all = [];
f_fluo_all = [];
x_fluo_all = [];
a_gr_all = [];
f_gr_all = [];
x_gr_all = [];
x_deathAge_all = [];

for gi = 1:numel(gene_list) 
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    clear frame_before_AB
    switch gene_list{gi}
        case 'gadX2'
            frame_before_AB = 171;
        case 'gadX3'
            frame_before_AB = 201;
        case 'gadX4'
            frame_before_AB = 199;
        case 'araC'
            frame_before_AB = 181;
        case 'recA1'
            frame_before_AB = 136;
        case 'recA2'
            frame_before_AB = 203;
        case 'W6_1'
            frame_before_AB = 203;
        case 'W6_2'
            frame_before_AB = 173;
        case 'bolA'
            frame_before_AB = 234;
    end
    
    % If there are multiple movies for the same reporter need to know how
    % to align the t = 0 points if antibiotics are added at different times
    if length(gene_list{gi}) >= 4 && strcmp(gene_list{gi}(1:4), 'gadX')
            frame_before_AB_min = 171;
    elseif length(gene_list{gi}) >= 4 && strcmp(gene_list{gi}(1:4), 'recA')
        frame_before_AB_min = 136;
    elseif strcmp(gene_list{gi}(1:2), 'W6')
        frame_before_AB_min = 173;
    else
        frame_before_AB_min = frame_before_AB;
    end

    a_fluo = [];
    f_fluo = [];
    x_fluo = [];
    a_gr = [];
    f_gr = [];
    x_gr = [];
    x_deathAge = [];
    
    %% Process each file:
    for fi = 1:numel(files)

        % Load file:
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        for chamber = 1:numel(res)
            
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
            
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames
            length_mother = res(chamber).lineage(1).length; % Measured length over those frames
            growthrate_mother = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,1); % Growth rate
            daughters_mother = res(chamber).lineage(1).daughters;
          
            
            if isempty(fluo_mother) || numel(fluo_mother) < frame_before_AB
%             if isempty(fluo_mother) || numel(fluo_mother) < 2 || ~isfield(res(chamber), 'outcome')
                continue
            end
            
            fluo_mother = smooth(fluo_mother, swindow)';
            growthrate_mother = smooth(growthrate_mother, swindow)';
            
% IF you use this code it allows incomplete traces to be included in the
% analysis. If so, use the "if isempty(fluo_mother) || numel(fluo_mother) < 2 || ~isfield(res(chamber), 'outcome')" code above
% Will need to switch to nanmean and nanstd for a bunch of the calculations
% below.
%                 fluo_mother_tmp = NaN(1,frame_before_AB);
%                 growthrate_tmp = NaN(1,frame_before_AB);
%                 fluo_mother_tmp(frames_mother) = fluo_mother;
%                 growthrate_mother_tmp(frames_mother) = growthrate_mother;
%                 fluo_mother = fluo_mother_tmp;
%                 growthrate_mother = growthrate_mother_tmp;
            
            outcome = res(chamber).outcome;       
            num_divisions_mother = numel(find(daughters_mother));
            
            if outcome == 'a'
                a_fluo = [a_fluo; fluo_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)]; % for all, crop to length of shortest movie
                a_gr = [a_gr; growthrate_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)];
            elseif outcome == 'f'
                f_fluo = [f_fluo; fluo_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)];
                f_gr = [f_gr; growthrate_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)];
            elseif outcome == 'x'
                x_fluo = [x_fluo; fluo_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)]; 
                x_gr = [x_gr; growthrate_mother(frame_before_AB-frame_before_AB_min+1:frame_before_AB)];
                x_deathAge = [x_deathAge; res(chamber).outcome_frame-frame_before_AB]; % age at death
            end
            
        end
    end
    
    if(normalization)
       allMeans = mean([f_fluo(:); x_fluo(:); a_fluo(:)]);
       f_fluo =  f_fluo./allMeans;
       x_fluo = x_fluo./allMeans;
       if ~isempty(a_fluo)
          a_fluo = a_fluo./allMeans; 
       end
    end
    
    a_fluo_all = [a_fluo_all; a_fluo];
    f_fluo_all = [f_fluo_all; f_fluo];
    x_fluo_all = [x_fluo_all; x_fluo];
    a_gr_all = [a_gr_all; a_gr];
    f_gr_all = [f_gr_all; f_gr];
    x_gr_all = [x_gr_all; x_gr];
    x_deathAge_all = [x_deathAge_all; x_deathAge];

    time_axis = (flip(frame_before_AB-frame_before_AB_min+1:frame_before_AB)-(frame_before_AB-frame_before_AB_min+1))*(-min_per_frame/60);
    Nsample = 6; % subsample the data for the heatmaps so that the image file size is smaller
end

clear a_fluo f_fluo x_fluo a_gr f_gf x_gr x_frameofdeath

percentSurvived = size(a_fluo_all,1)/(size(a_fluo_all,1)+  size(x_fluo_all,1)+  size(f_fluo_all,1))*100
percentDied = size(x_fluo_all,1)/(size(a_fluo_all,1)+  size(x_fluo_all,1)+  size(f_fluo_all,1))*100
numSurvivedDied = size(a_fluo_all,1)+size(x_fluo_all,1)

% -- Compare mean fluorescense of surving and dying cells
figure(1)
subplot(2,2,1); hold on
% shadedErrorBar(time_axis, mean(a_fluo_all), [prctile(a_fluo_all, 0.75); prctile(a_fluo_all, 0.25)], 'lineprops', '-g','patchSaturation',0.05)
% shadedErrorBar(time_axis, mean(x_fluo_all), [prctile(x_fluo_all, 0.75); prctile(x_fluo_all, 0.25)])
shadedErrorBar(time_axis, a_fluo_all, {@mean,@std}, 'lineprops', '-g','patchSaturation',0.05)
shadedErrorBar(time_axis, x_fluo_all, {@mean,@std})
xlabel('Time (h)')
ylabel('GFP (mean normalized)')
set(gca, 'XTick', -15:2.5:0)
axis([-14 0 0 5])
title('Fluorescence')

subplot(2,2,2); hold on
a_gr_smooth = [];
x_gr_smooth = [];
for i = 1:size(a_gr_all,1)
    a_gr_smooth(i,:) = smooth(a_gr_all(i,:),swindow);
end
for i = 1:size(x_gr_all,1)
    x_gr_smooth(i,:) = smooth(x_gr_all(i,:),swindow);
end
% shadedErrorBar(time_axis, mean(a_gr_smooth), [prctile(a_gr_smooth, 0.75); prctile(a_gr_smooth, 0.25)], 'lineprops', '-g','patchSaturation',0.05)
% shadedErrorBar(time_axis, mean(x_gr_smooth), [prctile(x_gr_smooth, 0.75); prctile(x_gr_smooth, 0.25)])
shadedErrorBar(time_axis, a_gr_smooth, {@mean,@std}, 'lineprops', '-g','patchSaturation',0.05)
shadedErrorBar(time_axis, x_gr_smooth, {@mean,@std})
xlabel('Time (h)')
ylabel('Growth rate (1/h)')
set(gca, 'XTick', -15:2.5:0)
axis([-14.25 0 -0.05 2])
title(gene_list{gi})
title('Growth Rate')

%T testing
for ii = 1:size(a_fluo_all,2)
    
    % Fluor t test
    x = a_fluo_all(:,ii);
    y = f_fluo_all(:,ii);
    [h,p] = ttest2(x,y); %assumes equal variance; add 'Vartype','unequal' for unequal
    if p < 0.001
        subplot(2,2,1); plot(time_axis(ii), 5, 'k.')
    end
    
    % Growth rate t test
    x = a_gr_all(:,ii);
    y = f_gr_all(:,ii);
    [h,p] = ttest2(x,y); %assumes equal variance; add 'Vartype','unequal' for unequal
    if p < 0.001
        subplot(2,2,2); plot(time_axis(ii), 2, 'k.')
    end
    
end

subplot(2,2,3); hold on
plot(time_axis, a_fluo_all, 'g'); hold on
plot(time_axis, x_fluo_all, 'k')
xlabel('Time (h)')
ylabel('GFP')
set(gca, 'XTick', -15:2.5:0)
axis([-14.25 0 0 5])
title('Fluorescence')


% -- Plot the fluorescense of individual surving and dying cells as heatmap
figure(2)
% [~,sidx] = sort(x_frameofdeath_all); % sort by time of death
[~,sidx] = sort(mean(x_fluo_all')); % sort by mean fluorescence values
% [~,sidx] = sort(x_fluo_all(:,end)); % sort end fluo value
subplot(2,2,1)
for i = 1:size(x_fluo_all,1)
    x = time_axis(3:Nsample:end); % start at 3 so it ends at the t = 0 timepoint
    y = i;
    z = zeros(size(x));
    c = x_fluo_all(sidx(i),3:Nsample:end);
    surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 2.75);
end
[~,sidx] = sort(mean(a_fluo_all')); % sort by mean fluorescence values
%     [~,sidx] = sort(a_fluo_all(:,end)); % sort end fluo value
for i = 1:size(a_fluo_all,1)
    x = time_axis(3:Nsample:end);
    y = i+100;
    z = zeros(size(x));
    c = a_fluo_all(sidx(i),3:Nsample:end);
    surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 2.75);
end
title('Fluor')
xlim([-14.25 0]); ylim([0 y])
set(gca, 'XTick', -12.5:2.5:0, 'YTick', 0:20:180)
caxis([0 2.5]); colorbar('XTick', 0:0.5:2.5)

% -- Plot the growth rate of individual surving and dying cells
subplot(2,2,3)
% [~,sidx] = sort(x_frameofdeath_all); % sort by time of death
[~,sidx] = sort(mean(x_fluo_all')); % sort by mean fluorescence values
% [~,sidx] = sort(x_fluo_all(:,end)); % sort end fluo value
for i = 1:size(x_gr_all,1)
    x = time_axis(3:Nsample:end);
    y = i;
    z = zeros(size(x));
    c = x_gr_all(sidx(i),3:Nsample:end);
    surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 2.75);
end
[~,sidx] = sort(mean(a_fluo_all')); % sort by mean fluorescence values
%     [~,sidx] = sort(a_fluo_all(:,end)); % sort end fluo value
for i = 1:size(a_gr_all,1)
    x = time_axis(3:Nsample:end);
    y = i+100;
    z = zeros(size(x));
    c = a_gr_all(sidx(i),3:Nsample:end);
    surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 2);
end
xlim([-14.25 0]); ylim([0 y])
set(gca, 'XTick', -12.5:2.5:0, 'YTick', 0:20:180)
caxis([0 1.75]); colorbar('XTick', 0:0.5:2)


figure(3)
subplot(2,2,1); hold on
a_fluo_crop = a_fluo_all(:,(end-averaging_window):end);
x_fluo_crop = x_fluo_all(:,(end-averaging_window):end);
a_gr_crop = a_gr_all(:,(end-averaging_window):end);
x_gr_crop = x_gr_all(:,(end-averaging_window):end);
scatter(mean(a_gr_crop,2), mean(a_fluo_crop,2), 'g')
s = scatter(mean(x_gr_crop,2), mean(x_fluo_crop,2));
s.MarkerEdgeColor = [0.5 0.5 0.5];
ag = a_gr_crop(:); 
af = a_fluo_crop(:);
xg = x_gr_crop(:); 
xf = x_fluo_crop(:);
plot(mean(ag), mean(af), 'gs')
plot(mean(xg), mean(xf), 'ks')
errorbar(mean(ag), mean(af), std(af), std(af), std(ag), std(ag), 'g') % errorbar(x,y,yneg,ypos,xneg,xpos)
errorbar(mean(xg), mean(xf), std(xf), std(xf), std(xg), std(xg), 'k') % errorbar(x,y,yneg,ypos,xneg,xpos)
xlabel("mean growth rate")
ylabel("mean GFP")
axis([0 2 0 6])
title('Dead or Alive')


subplot(2,2,2); hold on
colormap(winter)
c = x_deathAge_all;
scatter(mean(x_gr_all(:,(end-averaging_window):end),2), mean(x_fluo_all(:,(end-averaging_window):end),2), [], c, 'filled');
xlabel("mean growth rate")
ylabel("mean GFP")
axis([0 2 0 6])
title('Time of death')

figure(4)
subplot(2,2,1)
histogram((x_deathAge_all)*min_per_frame/60, 20, 'FaceColor', [0.5 0.5 0.5])
xlabel('Time to death (h)')
ylabel('Counts')


figure(5)
time_in_final_pulse_a = [];
for ii = 1:size(a_fluo_all,1) % loop through all the alive cell fluo traces
    idx = find(a_fluo_all(ii,:) < 1.5); % greater than the pulse threshold
    if ~isempty(idx)
        time_in_final_pulse_a = [time_in_final_pulse_a -time_axis(idx(end))];
    end
end
time_in_final_pulse_x = [];
for ii = 1:size(x_fluo_all,1) % loop through all the alive cell fluo traces
    idx = find(x_fluo_all(ii,:) < 1.5); % greater than the pulse threshold
    if ~isempty(idx)
        time_in_final_pulse_x = [time_in_final_pulse_x -time_axis(idx(end))];
    end
end
subplot(4,4,[1 2]); ha = histogram(time_in_final_pulse_a); xlim([0 14.25]); ylim([0 0.4]); set(gca, 'XTick', 0:2.5:15)
subplot(4,4,[5 6]); hx = histogram(time_in_final_pulse_x); xlim([0 14.25]); ylim([0 1]); set(gca, 'XTick', 0:2.5:15)
ha.BinWidth = 1.5;
hx.BinWidth = 1.5;
ha.Normalization = 'probability';
hx.Normalization = 'probability';
ha.FaceColor = 'g';
hx.FaceColor = [0.5 0.5 0.5];



% %Quartile-based approach for comparing cells' time of death
% figure(5)
% [sorted_outcomes, I] = sort(x_deathAge);
% quartile = floor(0.25*numel(x_deathAge));
% high_cutoff = I((end-quartile+1):end);
% low_cutoff = I(1:quartile);
% 
% high = x_fluo_all(high_cutoff,:);
% low = x_fluo_all(low_cutoff,:);
% 
% subplot(2,2,2); hold on
% shadedErrorBar(time_axis, high, {@mean,@std}, 'lineprops', '-c','patchSaturation',0.05)
% shadedErrorBar(time_axis, low, {@mean,@std})
% xlabel('Time (h)')
% ylabel('GFP (mean normalized)')
% set(gca, 'XTick', -15:2.5:0)
% axis([-14 0 0 3])







% if isequal(gene_list{gi}, 'gadX2')
%     subplot(2,2,3+gi); hold on
%     plot(time_axis, x_fluo([2 10 19 26],:), 'k')
%     plot(time_axis, a_fluo(2:5,:), 'g')
%     set(gca, 'XTick', -15:2.5:0)
%     axis([-14 0 0 5])
%     xlabel('Time (h)')
%     ylabel('GFP (mean normalized)')
% end
% 
% if isequal(gene_list{gi}, 'gadX2')
%     figure(9)
%     for i = 1:size(x_fluo,1)
%         subplot(9,9,i); plot(time_axis, x_fluo(i,:), 'k'); axis([-14 0 0 5])
%         [~, loc] = findpeaks(x_fluo(i,:), time_axis, 'MinPeakProminence', 0.5);
%         hold on
%         if ~isempty(loc)
%             plot(loc, 5, 'ko')
%         end
%     end
%     figure(10)
%     for i = 1:size(a_fluo,1)
%         subplot(9,9,i); plot(time_axis, a_fluo(i,:), 'g'); axis([-14 0 0 5])
%         [~, loc] = findpeaks(a_fluo(i,:), time_axis, 'MinPeakProminence', 0.5);
%         hold on
%         if ~isempty(loc)
%             plot(loc, 5, 'ko')
%         end
%     end
% end

% 
% if isequal(gene_list{gi}, 'gadX2')
%     subplot(2,2,3+gi); hold on
%     plot(time_axis, x_gr([2 10 19 26],:), 'k')
%     plot(time_axis, a_gr(2:5,:), 'g')
%     xlabel('Time (h)')
%     ylabel('growth rate (um/frame)')
%     set(gca, 'XTick', -15:2.5:0)
%     axis([-14 0 -6 8])
%     title(gene_list{gi})
% end
% 


% 
% 

% 
% if isequal(gene_list{gi}, 'gadX2')
%     figure(6)
%     subplot(2,2,1+gi); hold on
%     plot(time_axis, low([2 4 6 7],:), 'k')
%     plot(time_axis, high([1 5 7 8],:), 'c')
%     title(gene_list{gi})
%     set(gca, 'XTick', -15:2.5:0)
%     axis([-14 0 0 5])
%     xlabel('Time (h)')
%     ylabel('GFP (mean normalized)')
% end
% 
% %     figure(7)
% %     subplot(2,2,gi); hold on
% %     scatter(1*ones(1, size(a_fluo,1)), a_fluo(:,end), 'g')
% %     scatter(2*ones(1,size(x_fluo,1)), x_fluo(:,end), 'k')
% %     line([0.75 1.25],mean(a_fluo(:,end))*[1 1], 'Color', 'g')
% %     line([1.75 2.25], mean(x_fluo(:,end))*[1 1], 'Color', 'k')
% %     axis([0 3 0 6])
% %     ylabel('GFP (mean normalized)')
% %     title(gene_list{gi})
% 
% %     figure(8)
% %     subplot(2,2,gi); hold on
% %     scatter(1*ones(size(a_pulses)), a_pulses, 'g')
% %     scatter(2*ones(size(x_pulses)), x_pulses, 'k')
% %     line([0.75 1.25],mean(a_pulses)*[1 1], 'Color', 'g')
% %     line([1.75 2.25], mean(x_pulses)*[1 1], 'Color', 'k')
% %     axis([0 3 0 6])
% %     ylabel('Pulse height (mean normalized)')
% %     title(gene_list{gi})
% 
% %T testing
% num_to_avg = 1; %How many points before cipro addition to be averaged for t test
% 
% x = mean(a_fluo(:,(end-num_to_avg+1):end),2);
% y = mean(f_fluo(:,(end-num_to_avg+1):end),2);
% 
% disp(gene_list{gi})
% disp("Fluorescence T test")
% [h,p] = ttest2(x,y) %assumes equal variance; add 'Vartype','unequal' for unequal
% 
% disp("Growth rate T test")
% x = mean(a_gr(:,(end-num_to_avg+1):end),2);
% y = mean(f_gr(:,(end-num_to_avg+1):end),2);
% [h,p] = ttest2(x,y) %assumes equal variance; add 'Vartype','unequal' for unequal
% 
% disp("Quartile T test")
% x = mean(high(:,(end-num_to_avg+1):end),2);
% y = mean(low(:,(end-num_to_avg+1):end),2);
% [h,p] = ttest2(x,y) %assumes equal variance; add 'Vartype','unequal' for unequal
% 
% disp("Pulsing T tesT")
% x = a_pulses;
% y = x_pulses;
% [h,p] = ttest2(x,y) %assumes equal variance; add 'Vartype','unequal' for unequal
% 
% percentSurvived = size(a_fluo,1)/(size(a_fluo,1)+  size(x_fluo,1)+  size(f_fluo,1))*100
% percentDied = size(x_fluo,1)/(size(a_fluo,1)+  size(x_fluo,1)+  size(f_fluo,1))*100
% 
% %Logistic regression
% if isequal(gene_list{gi}, 'gadX2')
%     outcomes = [ones(size(a_fluo,1),1); zeros(size(x_fluo,1),1)]; %Make binomial vector of all outcomes
%     
%     %Selection of different variables to use for regression
%     endFluo = [a_fluo(:,end); x_fluo(:,end)]; %Fluorescence value immediately before cipro addition
%     endGr = [a_gr(:,end); x_gr(:,end)]; %growth rate immediately before cipro addition
%     maxFluo = [max(a_fluo,[],2); max(x_fluo,[],2)]; %Highest fluo value of a cell at any point in time
%     framesAboveFluoThreshold = numElThreshold([a_fluo; x_fluo], 1.15, '>'); %Amount of time cell spends with elevated fluorescence. Similar to pulse length, but doesn't require fluo to go back down
%     framesBelowGrThreshold = numElThreshold([a_gr; x_gr], 3.6, '<'); %Amount of time cell spends below growth rate threshold. 3.6 was chosen as a value that's ~ 85% of the mean GR
%     modPulseLength = modifiedPulseLength([a_fluo; x_fluo], 1.15)';
%     intPulseLength = trapz([a_fluo(:,130:end); x_fluo(:,130:end)].')';%Integrated pulse length
%     
%     %Set chosen regression variable as x to run regression model
%     x = intPulseLength;
%     
%     [b,dev,stats] = glmfit(x,outcomes,'binomial'); %Fit model
%     z = b(1) + (x*b(2)); %Use given model weights to find log-odds of p(alive)
%     z = 1./(1 + exp(-z)); %Convert p(alive) from log ratio to percent
%     
%     figure(13)
%     [sortedX, sortIndex] = sort(x); %Sort variable for plotting
%     sortedZ = z(sortIndex);
%     plot(sortedX, sortedZ); %Plot regression line
%     hold on;
%     scatter(x, outcomes, 'x') %Plot data being fitted
%     ylabel("p(alive)")
%     xlabel("Integrated fluorescence, frames 130-171 (end)")
%     text(0.1, 0.9, ['p = ',num2str(stats.p(2))]); %Overlay predictor p value
%     
%     scatter(-b(1)/b(2), 0.5, 'o', 'filled')
%     legend("logistic regression fit", "observed outcomes", "50% cutoff", 'Location', 'southeast')
%     
%     
%     %Find and overlay predictor classification success, using p(alive) = 50% as
%     %cutoff
%     if b(2) > 0
%         predictedOutcomes = x>(-b(1)/b(2)); %Gives 1 (alive) if cell if above threshold, 0 (dead) if not
%     else
%         predictedOutcomes = x<(-b(1)/b(2)); %Reverse logic for negative slopes
%     end
%     correctOutcomes = (predictedOutcomes == outcomes); %Gives 1 for every correct outcome, 0 for incorrect outcomes
%     accuracy = numel(find(correctOutcomes))/numel(predictedOutcomes); %Counts the number of correct outcomes, divides by total number of outcomes
%     text(0.1, 0.8, ['accuracy = ', num2str(accuracy)]); %Overlays accuracy on graph
%     
% end