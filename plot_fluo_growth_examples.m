close all; clear all

% basepath = '../mother_machine/data/'; % Directory where is data is located

% gene_list{1} = 'gadX_MG1655'; filename = '20210304_Position05.mat'; chamber = 10; 


basepath = '../mother_machine/data/'; % Directory where is data is located


gene_list{1} = 'gadX'; filename = '20190717_Position15.mat'; chamber = 9; % GOOD
% gene_list{1} = 'recA'; filename = '20190925_Position05.mat'; chamber = 11; % GOOD
% gene_list{1} = 'recA'; filename = '20190925_Position08.mat'; chamber = 4; % 06: maybe 9, 17; 07: 18; 08: 4 is quite good, maybe 15


% basepath = '../mother_machine/data_degradation_tags/';
% gene_list{1} = 'gadX_LAA'; filename = 'Position02.mat'; chamber = 14;


min_per_frame = 5; % minutes per frame
swindow = 5; % smoothing window
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera

left_color = [0/255 166/255 81/255]; %[38/255 173/255 23/255];
right_color = [236/255 0/255 140/255]; %[189/255 0/255 121/255];
PA_color = [0/255 140/255 190/255]; %[189/255 0/255 121/255];

for gi = 1:numel(gene_list)

    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.    
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    % Load file:
    load([basepath, gene_list{gi}, '/', filename]);
    disp([gene_list{gi}, ': Processing data: ', filename])
    
    
    if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
        continue
    end
    
    frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
    fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames:
    length_mother = res(chamber).lineage(1).length; % Measured length over those frames
    growthrate_mother = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,1); % Growth rate
    divisions_mother = res(chamber).lineage(1).daughters > 0; % Identify when divisions happen

% subplot(221); plot((frames_mother(20:end)-frames_mother(20))*5/60, fluo_mother(20:end));
% ylim([0 2e4])
% xlabel('Time (h)'); ylabel('GFP (a.u.)'); title('gadX (LAA tag)')
    
    
    % If frame_range is shorter than the full movie this will crop
    % the data, if not specified it defaults to the full movie.
    if exist('frame_range')
        frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
        fluo_mother = fluo_mother(frames_mother);
        length_mother = length_mother(frames_mother);
        growthrate_mother = growthrate_mother(frames_mother);
        divisions_mother = divisions_mother(frames_mother);
        
        Nframes = frame_range(end)-frame_range(1) + 1;
    else
        Nframes = size(res(1).labelsstack,3);
    end

    if numel(frames_mother) < 3 % Only work with data that has at least 3 points
        continue
    end
    
    % Approximate derivative of mother cell fluorescence
    fluo_mother = smooth(fluo_mother, swindow);
    dMdt = diff(fluo_mother);
    
    % Crop data so that it's the same length as the derivative data
    growthrate_mother = growthrate_mother(2:end);
    growthrate_mother = smooth(growthrate_mother, swindow);
    fluo_mother = fluo_mother(2:end);
    length_mother = length_mother(1:(end-1));
    time = (frames_mother(2:end)-frames_mother(2))*min_per_frame/60;
    
    if mean(growthrate_mother) < 0.5 % Quality control - don't work with cells that aren't growing
        continue
    end
    
    A = fluo_mother;
    
    % Derivative calculations
    M = fluo_mother;
    L = length_mother';
    
    dMdt = (M(3:end) - M(1:end-2))/(2*min_per_frame/60);
    dLdt = (L(3:end) - L(1:end-2))/(2*min_per_frame/60);
    
    dLdt = remove_neg(dLdt);
    
    M = M(2:end-1);
    L = L(2:end-1);
    mu = growthrate_mother(2:end-1);
    
    p = 0.1;
    PA = M.*((1./L).*dLdt + p) + dMdt; % promoter activity
    
    PA = smooth(PA, swindow);
    mu = smooth(mu, swindow);
    
    % Plotting Code    
    % ----------------- GFP and growth rate
    subplot(221)
    colororder([left_color; right_color])
    yyaxis left
    plot(time, A/mean(A)); hold on
    ylabel('Fluorescence (mean normalized)')
    xlabel('Time (h)')
    axis([0 (Nframes-1)*min_per_frame/60 0 2.2])
    
    yyaxis right
%     plot(time, growthrate_mother)
    plot(time, smooth(growthrate_mother, swindow))
    ylabel('Growth rate (1/h)','rotation',270, 'VerticalAlignment','bottom', 'HorizontalAlignment','center')
    axis([0 (Nframes-1)*min_per_frame/60 0 3])
    yticks([0:1:3])
    
    for div = find(divisions_mother(2:end)) % Go through indexes where there has been a division (see what the find function does)
        plot(time(div), 2.9, ' .k'); % For division indexes, plot a single cirle
    end
    
%     ----------------- Promoter activity and growth rate
    figure(2)    
    subplot(221)
    colororder([PA_color; right_color])
    yyaxis left
    plot(time(3:end-2), PA(2:end-1)/mean(PA(2:end-1))); hold on
    ylabel('Promoter Activity (mean normalized)')
    xlabel('Time (h)')
    axis([0 (Nframes-1)*min_per_frame/60 0 2.5])
    
    yyaxis right
    plot(time, smooth(growthrate_mother, swindow))
    ylabel('Growth rate (1/h)','rotation',270, 'VerticalAlignment','bottom', 'HorizontalAlignment','center')
    axis([0 (Nframes-1)*min_per_frame/60 0 3])
    yticks([0:1:3])
        
    for div = find(divisions_mother(2:end)) % Go through indexes where there has been a division (see what the find function does)
        plot(time(div), 2.9, ' .k'); % For division indexes, plot a single cirle
    end
        
    clear frame_range
end
