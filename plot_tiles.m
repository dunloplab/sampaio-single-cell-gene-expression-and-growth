close all; clear all

basepath = '../mother_machine/data/'; % Directory where data is located
% basepath = '../mother_machine/data_delta_rpoS/'; % Directory where data is located

folders = dir(basepath); % Find subfolders that contain data
folders = folders(~startsWith({folders.name}, '.')); % Remove hidden files
folders = folders([folders.isdir]); % Only include directories

% NOTE: to avoid a bad looking trace or two I've put "reorder-" in front of
% some of the file names so they'll be used later and not appear in the 25.

% gene_list = {folders.name}; % Capture gene names; Use this code to run all data sets
gene_list{1} = 'gadW';
% gene_list{1} = 'gadX_MG1655';
% gene_list{2} = 'gadX_delta_rpoS';
% gene_list{1} = 'gadX_integrated';
% gene_list{1} = 'bolA'; % Use this code instead if you want to run just one (or a few) data sets -- comment out gene_list code above
% gene_list{2} = 'fur';
% gene_list{3} = 'acrAB';
% gene_list{4} = 'rpoH';

min_per_frame = 5;
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera

swindow = 5; % smoothing window

clist = jet(numel(gene_list)); % generate color list
Nshow = 25; % number of replicates to show

Nsample = 6; % subsample the data so that the image file size is smaller

for gi = 1:numel(gene_list)
    
    ypos = 1;
    
    % If frame_range is not defined here (returns NaN), code will default to the full length of the movie.    
    if ~isnan(frame_range_settings(gene_list{gi}, basepath))
        frame_range = frame_range_settings(gene_list{gi}, basepath);
    end
    
    files = dir([basepath, gene_list{gi}, '/*.mat']); % Find all .mat files in the subfolder
    
    %% Process each file:
    for fi = 1:numel(files)
        
        if ypos > Nshow % we already have enough data plotted, don't bother loading
            continue
        end
        
        autox_all = [];
        autoy_all = [];
        crossxy_all = [];
        
        fluo_mother_all = [];
        length_mother_all = [];
        growthrate_mother_all = [];
        
        % Load file:
        load([basepath, gene_list{gi}, '/', files(fi).name]);
        disp([gene_list{gi}, ': Processing data: ', files(fi).name])
        
        % For each chamber, plot the mother cell fluorescence, growth rate & divisions:
        for chamber = 1:numel(res)
            if isempty(res(chamber).lineage) % if there's nothing in the chamber, skip it; jump to next chamber
                continue
            end
            
            frames_mother = res(chamber).lineage(1).framenbs; % The frames that the cell is present
            fluo_mother = res(chamber).lineage(1).fluo1; % Measured fluorescence level over those frames:
            length_mother = res(chamber).lineage(1).length; % Measured length over those frames
            growthrate_mother = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,1); % Growth rate
            divisions_mother = res(chamber).lineage(1).daughters > 0; % Identify when divisions happen
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames_mother = intersect(frames_mother, frame_range) - frames_mother(1) + 1;
                fluo_mother = fluo_mother(frames_mother);
                length_mother = length_mother(frames_mother);
                growthrate_mother = growthrate_mother(frames_mother);
                divisions_mother = divisions_mother(frames_mother);
                
                Nframes = frame_range(end)-frame_range(1) + 1;
            else
                Nframes = size(res(1).labelsstack,3);
            end
            
            if numel(frames_mother) < 3 % Only work with data that has at least 3 points
                continue
            end
            
            % Approximate derivative of mother cell fluorescence
            fluo_mother = smooth(fluo_mother, swindow);
            
            % Crop data so that it's the same length as the derivative data
            growthrate_mother = growthrate_mother(2:end);
            growthrate_mother = smooth(growthrate_mother, swindow);
            fluo_mother = fluo_mother(2:end);
            length_mother = length_mother(1:(end-1));
            time = (frames_mother(2:end)-frames_mother(2))*min_per_frame;
            
            if mean(growthrate_mother) < 0.5 % Quality control - don't work with cells that aren't growing
                continue
            end
            
            % Plot colorbars to visualize fluorescense. Commenting this code out speeds up run times.
            cropsize = 181;
            % cropsize = 128;
            
            if length(time) >= cropsize && ypos <= Nshow
                figure(1)
                subplot(3,5,gi)
                x = time(1:cropsize)/60;
                x = x(1:Nsample:end); % reduce size of image by using a subset of the data
                y = ypos;
                z = zeros(size(x));
%                 c = fluo_mother(1:Nsample:cropsize)'/mean(fluo_mother);
% c = fluo_mother(1:Nsample:cropsize)'/1.5; % for comparing W6 (1.5) and W6_delta_rpoS (1)
% c = fluo_mother(1:Nsample:cropsize)'/3.3; % for comparing evgA (1) and evgA_delta_rpoS (3.3)
c = fluo_mother(1:Nsample:cropsize)'/1; % for comparing gadW (1) and gadW_delta_rpoS (3.3)
                surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 2.75);
%                 caxis([500 5000])
%                 caxis([500 2500])
                caxis([500 2000])
%                 caxis([0.25 1.75])
                title(gene_list{gi})
%                 xlabel('Time (h)')
%                 ylabel('Fluo / mean(Fluo)')
                set(gca, 'XTick', 0:5:241*min_per_frame/60, 'YTick', 0:5:Nshow)
%                 xlim([0 241*min_per_frame/60])
                xlim([0 cropsize*min_per_frame/60])
                ylim([0 Nshow])
                ypos = ypos + 1;
                % colorbar
                
                
                % Plot representative example
                if isequal(gene_list{gi}, 'gadX')
                    if isequal(files(fi).name, '20190717_Position13.mat') && chamber == 16
%                     if isequal(files(fi).name, '20190717_Position15.mat') && chamber == 2
                        figure(2); subplot(3,3,[1 2])
                        plot(x, c);
                        title(['Chamber #: ', num2str(chamber), ' ', files(fi).name, ' ypos = ', num2str(ypos)])
                        axis([0 15 0 2.5])
                        
                        subplot(3,3,[4 5])
                        surface([x;x],[0;0],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 11);
                        caxis([0.25 1.75])
                        xlim([0 15])
                    end
                    
                    figure(3)
                    subplot(2,2,1)
                    surface([x;x],[y;y],[z;z],[c;c], 'facecol', 'no', 'edgecol', 'interp', 'linew', 8);
                    caxis([0.25 1.75])
                    title(gene_list{gi})
                    xlabel('Time (h)')
                    ylabel('Fluo / mean(Fluo)')
                    xlim([0 15])
                    ylim([0 25])
                    colorbar
                    
                end
            end
            
        end
    end
    clear frame_range
end

% colorbar
