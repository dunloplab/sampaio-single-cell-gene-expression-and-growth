function x = remove_neg(x)
neg_idx_list = find(x <= 0);
pos_idx_list = find(x > 0);

for i = 1:length(neg_idx_list) % for all negative values, replace with average of nearest two non-negative values (one before, one after)
    idx = neg_idx_list(i);
    
    before_list = find(pos_idx_list < idx); % find the last good value before the negative value
    if ~isempty(before_list)
        before = pos_idx_list(before_list(end));
    else
        before = idx; % not correct, but will be rare (e.g. dLdt(1) <= 0) and will avoid errors
    end
    
    after_list = find(pos_idx_list > idx); % find the first good value after the negative value
    if ~isempty(after_list)
        after = pos_idx_list(after_list(1));
    else
        after = idx; % not correct, but will be rare (e.g. dLdt(end) <= 0 and will avoid errors
    end
    
    x(idx) = 0.5*(x(before) + x(after));
end

end