clear all, close all

load Silander_data.mat

subplot(3, 3, [1 2 4 5])
plot(1./mean_CV2_std(:,1), mean_CV2_std(:,2), 'b.')

idx_list = [
    1296 % araC	b0064
    1173 % bolA	b0435
    378 % dinB	b0231
    1491 % evgA	b2369
    1483 % gadW	b3515
    1507 % gadX	b3516
    1132 % metJ	b3938
    992 % oxyR	b3961
    1093 % phoP	b1130
    1287 % recA	b2699
    1457 % rob	b4396
    1448 % rpoH	b3461
    503 % sulA	b0958
    ];

hold on

for i = 1:numel(idx_list)
    plot(mean_CV2_std(idx_list(i), 1), mean_CV2_std(idx_list(i), 2), 'ro', 'MarkerSize', 5)
end
xlabel('1/Mean'); ylabel('CV^2'); title('Silander, et al. data')
xlim([0 0.41])

mean_CV2_std_Sampaio = [ % Note: these are normalized by the exposure times
61.768811	0.023590745	9.487244		% arac	b0064
33.812454	0.050149051	7.5719555		% bolA	b0435
16.42904533	0.027471604	2.723042		% dinB	b0231
548.50761	0.040250069	110.0439		% evgA	b2369
265.415755	0.071181067	70.81234		% gadW	b3515
996.78782	0.07064374	264.93514		% gadX	b3516
47.942964	0.02330435	7.318859		% metJ	b3938
54.961455	0.027834953	9.169665		% oxyR	b3961
44.154917	0.018070265	5.935555		% phoP	b1130
178.713341	0.116653346	61.038742		% recA	b2699
247.24073	0.049852886	55.20331667		% rob	b4396
232.0105	0.049186345	51.45527667		% rpoH	b3461
17.910832	0.031475978	3.177645		% sulA	b0958
];

figure(2)
subplot(221); plot(mean_CV2_std_Sampaio(:, 1), mean_CV2_std(idx_list(:),1), 'k.'); xlabel('Mean (this study)'); ylabel('Mean (Silander, et al.)')
subplot(222); plot(mean_CV2_std_Sampaio(:, 3), mean_CV2_std(idx_list(:),3), 'k.'); xlabel('Std. (this study)'); ylabel('Std. (Silander, et al.)')

% Linear regression
val_list = [1 3];

for i = 1:numel(val_list)
    x = mean_CV2_std_Sampaio(:, val_list(i));
    y = mean_CV2_std(idx_list(:),val_list(i));
    p = polyfit(x, y, 1);
    yfit = polyval(p,x);
    yfit =  p(1) * x + p(2);
    yresid = y - yfit;
    SSresid = sum(yresid.^2);
    SStotal = (length(y)-1) * var(y);
    rsq = 1 - SSresid/SStotal
    
    figure(2); subplot(2, 2, i); hold on; plot(x, yfit)
end


% Supplementary Table
for i = 1:numel(idx_list)
    disp([num2str(mean_CV2_std(idx_list(i), [1 3])), ' ', num2str(mean_CV2_std_Sampaio(i, [1 3]))])
end