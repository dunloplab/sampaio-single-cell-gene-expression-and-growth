clear all, close all


min_per_frame = 5;
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera
swindow = 5; % smoothing window

data = [];

% --- GadX ---
files = dir('../mother_machine/data/gadX/*.mat'); % Find all .mat files in the subfolder
frame_range = 24:205; % crop off the start of the movie b/c fluor values are higher there; data is from two dates so this is the shorter of the two


%% Process each file
for fi = 1:numel(files)
    
    % Load file:
    load(['../mother_machine/data/gadX/', files(fi).name]);
    disp(['Processing data: ', files(fi).name])
    
    for chamber = 1:numel(res)
        [chamber_height, chamber_width, ~] = size(res(chamber).labelsstack);
        
        clear vertical_centroid_mother
        for cell = 1:numel(res(chamber).lineage)
            
            frames = res(chamber).lineage(cell).framenbs; % The frames that the cell is present
            fluo = res(chamber).lineage(cell).fluo1; % Measured fluorescence level over those frames:
            length = res(chamber).lineage(cell).length; % Measured length over those frames
            growthrate = (60/min_per_frame)*compute_growthrate_log(res(chamber).lineage,cell); % Growth rate
            pixels = res(chamber).lineage(cell).pixels;
            area = res(chamber).lineage(cell).area;
            
            % If frame_range is shorter than the full movie this will crop
            % the data, if not specified it defaults to the full movie.
            if exist('frame_range')
                frames = intersect(frames, frame_range) - frames(1) + 1;
                fluo = fluo(frames);
                length = length(frames);
                growthrate = growthrate(frames);
                growthrate = smooth(growthrate, swindow);
                pixels = pixels(frames);
                area = area(frames);
            end
            
            vertical_centroid = [];
            for i = 1:numel(pixels)
                mask = zeros(chamber_height, chamber_width);
                mask(pixels{i}) = 1;
                s = regionprops(mask, 'centroid');
                vertical_centroid(i) = s.Centroid(2); % the vertical position of the centroid
            end
            if cell == 1 % the mother cell
                vertical_centroid_mother = mean(vertical_centroid); % the avereage verical centroid value over all the frames
            end
            vertical_centroid = vertical_centroid - vertical_centroid_mother; % subtract the offset; assumes chambers don't drift much
            
            data_tmp = [vertical_centroid' growthrate area' numel(frames)*ones(size(growthrate)) length'];
            data = [data; data_tmp];
            
        end
    end
end


data = data(data(:,3) > 100, :); % Only consider data where cells are above this area threshold in size
data = data(data(:,4) > 5, :); % Only consider data where we have at least this many frames

data(:,1) = data(:,1)*um_per_pixel; % Convert pixels to length
data(:,5) = data(:,5)*um_per_pixel;

subplot(3,2,1);
vals = -0.25:1.5:17;
for i = 2:numel(vals)
    databin = data(data(:,1) > vals(i-1) & data(:,1) <= vals(i),:);
    mbin(i-1) = median(databin(:,2));
    sbin(i-1) = std(databin(:,2));
end
plot(data(1:50:end, 1), data(1:50:end, 2), '.', 'Color', 0.85*[1 1 1]); hold on
% errorbar(vals(2:end), mbin, sbin)
plot(vals(2:end), mbin, 's-')
axis([0 17.25 -0.5 3.5])
xlabel('Position in the chamber (um)')
ylabel('Growth rate (1/h)')
clear mbin sbin

subplot(3,2,2);
vals = 2:0.5:6;
for i = 2:numel(vals)
    databin = data(data(:,5) > vals(i-1) & data(:,5) <= vals(i),:);
    mbin(i-1) = median(databin(:,2));
    sbin(i-1) = std(databin(:,2));
%     plot(0.1*randn(size(databin(1:100:end,2)))+vals(i), databin(1:100:end,2), '.', 'Color', 0.85*[1 1 1]); hold on
end
plot(data(1:200:end, 5), data(1:200:end, 2), '.', 'Color', 0.85*[1 1 1]); hold on
% errorbar(vals(2:end), mbin, sbin)
plot(vals(2:end), mbin, 's-')
axis([1.5 6.5 -0.5 3.5])
xlabel('Cell length (um)')
ylabel('Growth rate (1/h)')