function [growth] = compute_growthrate_log(lineage,cell_nb)

cell = lineage(cell_nb);
for frame_index = 1:numel(cell.framenbs)
    growth(frame_index) = growth_at_timepoint(lineage,frame_index,cell_nb);
end
    

function growth = growth_at_timepoint(lineage,frame_index,cellnb)

framenb = lineage(cellnb).framenbs(frame_index);

if cellnb == 0
    growth = NaN;
else
    % -- 1. The first frame in the lineage
    if frame_index == 1 
        growth = NaN;
        % It's the first frame in the entire movie - could add later
        % It's not the first frame in the entire movie - could add later
        
    % -- 2. The last frame in the lineage
    elseif frame_index == numel(lineage(cellnb).framenbs) 
        growth = NaN;
        % It's the last frame in the entire movie - could add later
        % It's not the last frame in the entire movie - could add later      
    
    % -- 3. In the middle of the lineage
    else
        daughternbs = lineage(cellnb).daughters(frame_index:frame_index+1);
        
        if sum(daughternbs) == 0 % the cell has not divided during the frame_index-1 to frame_index+1 timeframe
            growth = 1/2*log(lineage(cellnb).length(frame_index+1) / lineage(cellnb).length(frame_index-1)); % log(future length/previous length)
        
        elseif prod(daughternbs) == 0 % it has divided (otherwise the previous elseif would catch it), but only one division
            if daughternbs(1) ~= 0 && numel(lineage(sum(daughternbs)).length) >= 2 % division happened at frame_index AND daughter information exists for frame_index+1
                growth = 1/2*log((lineage(cellnb).length(frame_index+1) + lineage(sum(daughternbs)).length(2)) / lineage(cellnb).length(frame_index-1)); % log(future length of mother + daughter /previous length)    
            else % division happened at frame_index+1 OR we don't have the daughter info for frame_index+1
                growth = 1/2*log((lineage(cellnb).length(frame_index+1) + lineage(sum(daughternbs)).length(1)) / lineage(cellnb).length(frame_index-1)); % log(future length of mother + daughter /previous length)        
            end
            
        else % it divided at frame_index and frame_index+1 (very unlikely), or something unexpected has happened
            growth = NaN;
        end
    end
    if isempty(growth) % This can occassionally happen if there are issues with cell's mother
        growth = NaN;
    end
end