clear all, close all

%%%Inputs start

% %---- Alive/Dead kymographs
% dir_info = '../../mother_machine/movies/alive_dead/20200223_p1to15_f1to337/';
% merge = false; % Set this to true to merge the phase and gfp channels, or false to have them display separately
% position = '12'; % Set position number
% % 
% % box = [278    90    19   155]; % alive cell [XMIN YMIN WIDTH HEIGHT]
% % vid_name = 'gadX_cipro_alive';
% box = [546    90    19   155]; % dead cell  [XMIN YMIN WIDTH HEIGHT]
% vid_name = 'gadX_cipro_dead';
% % box = [232 90 75 155]; % alive and dead  [XMIN YMIN WIDTH HEIGHT]
% % box = [193 90 115 165]; % alive and dead  [XMIN YMIN WIDTH HEIGHT]
% % vid_name = 'gadX_cipro_both';
% 
% pbound = [1500 10000]; % Color bounds to use for phase image
% gbound = [0 7000];   % and gfp image
% 
% tiff_stack = false;
% % % tlist = 1:6:250; % List of frame numbers to use in the filmstrip
% tlist = 1:250; % List of frame numbers to use in the movie

% ---- Representative kymographs

box_width = 27;

% % -- AraC
% % dir_info = '../../mother_machine/movies/20200302_arac_p8_f1to436_ciproatf182.tif';
% dir_info = '../../mother_machine/movies/araC_stacks_20190911_p5.mat';
% % box = [275   73   box_width 205]; % for filmstrip
% box = [270   73   box_width 205]; % for movie
% g_mean_val = 1021; % Can get this number from plot_snapshot_data.m
% pbound = [1500 10000]; % Color bounds to use for phase image
% vid_name = 'araC_movie';

% % -- GadX
% dir_info = '../../mother_machine/movies/gadX_stacks_20190722_p1.mat';
% box = [277 87  box_width 205]; % gadX [XMIN YMIN WIDTH HEIGHT]
% g_mean_val = 1115; % Can get this number from plot_snapshot_data.m
% pbound = [0 6000]; % Color bounds to use for phase image
% vid_name = 'gadX_movie';

% -- RecA
% dir_info = '../../mother_machine/movies/recA_stacks_20190925_p1_rotated-3deg.mat'; % for filmstrip (rotated image)
% box = [29 94 box_width 185]; % for filmstrip (rotated image)
dir_info = '../../mother_machine/movies/recA_stacks_20190925_p1.mat'; % for movie (non-rotated image)
box = [10 94 box_width 190]; % for movie (non-rotated image)
g_mean_val = 2933; % Can get this number from plot_snapshot_data.m -- yes, that script also has the MM averages
pbound = [1500 10000]; % Color bounds to use for phase image
vid_name = 'recA_movie';

% tlist = 1:181; % List of frame numbers to use in the movie
tlist = 2:6:182; % List of frame numbers to use in the filmstrip

gbound = g_mean_val*[0.5 2.5];   % and gfp image
tiff_stack = true;

% 
% %%%Inputs end

min_per_frame = 5;
um_per_pixel = 0.129; % microns / pixel (2x2 binning) Clara camera


% if it's a tiff stack separate it into phase and gfp stacks
if tiff_stack

% % Use this code if you need to read in from a tiff stack. I ended up
% % separating phase and gfp and resaving as a mat file because this was
% % slow.
%     imP_stack = [];
%     imG_stack = [];
%     tiff_info = imfinfo(dir_info);
%     disp('Reading in the tiff stack')
%     for i = 1:size(tiff_info,1)
%         temp_tiff = imread(dir_info, i);
% %         temp_tiff = imrotate(temp_tiff, -3);  % if you need to rotate the image
%         if rem(i, 2) % odd numbers
%             imP_stack = cat(3, imP_stack, temp_tiff);
%         else % even numbers
%             imG_stack = cat(3, imG_stack, temp_tiff);
%         end    
%     end
%     save recA_stacks_20190925_p1 imP_stack imG_stack % this will save in the code directory, just move it to the movie dir

    load(dir_info)
    
%     % use this code if you want to draw the cropping box by hand
%     imshow(imP_stack(:,:,1),[]);
%     h = imrect;
%     disp('draw a cropping box')
%     box = wait(h)
end

vid = VideoWriter(vid_name, 'MPEG-4');
vid.FrameRate = 10;
vid.Quality = 100;
open(vid);

scale_bar_size5 = round(5/um_per_pixel); % 5um in pixels
scale_bar_size2 = round(2/um_per_pixel); % 2um in pixels

filmstrip = []; % initialize the filmstrip to be empty
for ti = 1:length(tlist)
    
    % Read in the images
    if tiff_stack
        imP = imP_stack(:,:,tlist(ti));
        imG = imG_stack(:,:,tlist(ti));
    else
        imP = imread([dir_info, 'Position', position, '_Channel01.tif'], tlist(ti)); % may need to change this to strN(.., 2) if format is t01, t02, etc. also, may need to change to _c1.tif
        imG = imread([dir_info, 'Position', position, '_Channel02.tif'], tlist(ti));
    end
    
    imP = double(imP);
    imG = double(imG);
    
    % Crop the images
    imP = imcrop(imP,box);
    imG = imcrop(imG,box);
    
    % Set the thresholds for the colors
    imP(imP < pbound(1)) = pbound(1);
    imG(imG < gbound(1)) = gbound(1);
    imP(imP > pbound(2)) = pbound(2);
    imG(imG > gbound(2)) = gbound(2);
    imP = (imP-pbound(1))/(pbound(2)-pbound(1));
    imG = (imG-gbound(1))/(gbound(2)-gbound(1));
    
    % Make rgb images for phase contrast and fluorescense images
    phase = makergb(imP, imP, imP, 0); % the final zero indicates that it should not be normalized (we've already normalized here)
    rgb = makergb(zeros(size(imG)), imG, zeros(size(imG)), 0);
    
%     snap = rgb;
%     snap = phase + rgb; % use this to merge phase and gfp image
    snap = [phase; rgb];
    
    filmstrip = [filmstrip snap];
    
    snap = [phase rgb];
    if isequal(vid_name(1:10), 'gadX_cipro')
        tval = min_per_frame*(tlist(ti))/60 - 52*5/60;
        disp(['t = ', num2str(tval)])
        if tval >= 0
            snap = insertText(snap, [1 size(snap,1)-25], datestr(tval/24, 'HH:MM'), 'FontSize', 11, 'TextColor', 'white', 'BoxOpacity', 0);
        else
            snap = insertText(snap, [1 size(snap,1)-25], ['-', datestr(24 - (tval/24), 'HH:MM')], 'FontSize', 11, 'TextColor', 'white', 'BoxOpacity', 0);
        end
    else
        tval = min_per_frame*(tlist(ti)-1)/60;
        disp(['t = ', num2str(tval)])
        snap = insertText(snap, [1 size(snap,1)-25], datestr(tval/24, 'HH:MM'), 'FontSize', 11, 'TextColor', 'white', 'BoxOpacity', 0);
    end
    
    
%     if isequal(vid_name(1:10), 'gadX_cipro') && tlist(ti) >= 52 && tlist(ti) <= 59
%         snap = insertText(snap, [1 1], 'CIP', 'FontSize', 11, 'TextColor', 'red', 'BoxOpacity', 0);
%     end

    if isequal(vid_name(1:10), 'gadX_cipro')
        stress_bar = ones(15, size(snap,2));
        colored_stress_bar(:, :, 1) = 255*stress_bar;
        colored_stress_bar(:, :, 2) = 255*stress_bar;
        colored_stress_bar(:, :, 3) = 255*stress_bar;
        if tlist(ti) >= 52 && tlist(ti) <= 59
            colored_stress_bar(:, :, 1) = 255*stress_bar;
            colored_stress_bar(:, :, 2) = 0*stress_bar;
            colored_stress_bar(:, :, 3) = 0*stress_bar;
        end
        snap = [colored_stress_bar; snap];
    end


    snap(end-8:end-6, 3+(1:scale_bar_size2), :) = 256; % White bar
    
    writeVideo(vid, snap)
    

end

filmstrip(end-8:end-6, 3+(1:scale_bar_size5), :) = 256; % White bar

figure(1)
imshow(filmstrip)
truesize
% print('filmstrip','-dpdf','-bestfit')

close(vid);

% figure(2)
% stress_graphic = [];
% for ti = 1:length(tlist)
%     width = box(3)+1;
%     if tlist(ti) >= 52 && tlist(ti) <= 59
%         stress_graphic = [stress_graphic ones(15, width)];
%     else
%         stress_graphic = [stress_graphic zeros(15, width)];
%     end
% end
% imshow(stress_graphic)
% truesize

